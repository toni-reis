\chapter{Analisi funzionale e Progettazione}

\begin{figure}
\centering
\includegraphics[width=14cm]{Classi_Alto_Livello}
\caption{Architettura ad alto livello del sistema.}
\label{fig:classi}
\end{figure}

Nella figura \ref{fig:classi} è riportato il diagramma delle classi di alto livello, con le funzioni essenziali per il funzionamento del programma. Segue una descrizione dettagliata 
dei vari componenti.

\section{Track}
\label{Progettazione:Track}
Il componente \textit{Track} rappresenta il circuito, nel quale è racchiuso il cuore della simulazione; la maggior parte delle problematiche relative alla concorrenza riguardano questo componente.

Il tracciato è implementato come un insieme di oggetti di tipo \textit{Sector}, che rappresentano i settori di cui è composta la pista. Ogni settore presenta caratteristiche di lunghezza, curvatura, condizione meteo e molteplicità. Per molteplicità si intende il numero di ``file'' che possono formarsi all'interno del settore. Questo concetto è implementato associando ad ogni settore un numero di \textit{Lane} o \textit{corsie} pari alla molteplicità. Auto che percorrono lo stesso settore in \textit{lane} diversi hanno la possibilità di superarsi. 
Un settore può inoltre contenere i box (che sono implementati come un settore a parte) con proprie caratteristiche di lunghezza e velocità di percorrenza. Il settore box si intende avere molteplicità pari al numero massimo di auto in gara, dato che le macchine possono superarsi in dipendenza al tipo e alla durata della sosta effettuata. La velocità di percorrenza dei box è invece fissata, come da regolamento Formula 1, a 80 km/h. 

Il componente \textit{track} è implementato come una \textit{Remote Call Interface}, con il risultato che ogni chiamata alle procedure viene eseguita da un \textit{task} anonimo, che agisce in conto e per conto di \textit{Car}, che è anche l'unico componente ad eseguire queste chiamate.
Il metodo principale è denominato \textit{PutOnPitLane}, con il quale un'auto si presenta al circuito e comunica che è pronta a partecipare alla simulazione. La chiamata a questo metodo ritorna solo quando la corsa dell'auto chiamante è terminata. Nello svolgimento della simulazione, l'auto deve comunque comunicare gli aggiornamenti delle caratteristiche tecniche. Le modalità con cui è stata risolta questa problematica sono analizzate in seguito.
\\
La realizzazione del circuito, deve rispondere principalmente a due problematiche:
\begin{itemize}
	\item \textbf{concorrenza}: dare la possibilità a tutti i concorrenti di partecipare alla competizione rispettando però le regole di accesso e accodamento ai vari tratti. Deve fornire, per quanto possibile, il parallelismo delle esecuzioni delle varie auto. I problemi da evitare sono quelli classici della concorrenza ovvero \textit{race condition}, \textit{deadlock}, \textit{starvation} etc.
	\item \textbf{coerenza}: le modalità con cui le auto accedono ai settori e i tempi/velocità con cui vengono percorsi devono essere coerenti, rispetto all'affollamento degli stessi e rispetto al modello fisico della simulazione. Ad esempio: un'auto veloce che è preceduta da un'auto lenta e non ha possibilità di sorpasso (stesso \textit{lane}), avrà la stessa velocità di percorrenza dell'auto che la precede. Analogamente auto dalle stesse caratteristiche di prestazioni e usura, avranno le stesse performance. 
\end{itemize}
Le caratteristiche del circuito sono configurabili tramite file \textit{XML} e caricate all'avvio della simulazione. Prima del caricamento vengono eseguiti dei controlli sia sul formato del file sia sulla sintassi dello stesso. Questa validazione avviene per mezzo di un file \textit{XML Schema} denominato \texttt{schema.xsd} e incluso tra i file di configurazione del progetto.


\subsection{Modello fisico}
\label{Progettazione:Track:Modello_Fisico}
In un progetto come quello assegnato, il modello fisico utilizzato è molto importante: più esso è accurato e rispecchia la realtà, più la simulazione risulta simile ad una corsa reale. 
Il modello fisico utilizzato in questo prototipo è molto semplificato rispetto alla realtà, ma risulta essere adeguato agli scopi del simulatore. Si tengono in considerazione diversi fattori legati all'usura, oltre alle caratteristiche tecniche delle auto. Il modello cerca di rispecchiare il più possibile quello che accade nelle gare di formula uno reali.
Generalmente, i primi giri non sono veloci poiché le auto sono molto pesanti per la quantità di carburante caricata.
Man mano che ci si avvicina al \textit{pit stop}, le auto riesco ad effettuare giri più veloci essendo rimaste con poco carburante a bordo. 
Questi tempi sono comunque maggiori rispetto ai tempi delle qualificazioni, ottenuti con poco carburante e con gomme non usurate.
Queste osservazioni ci hanno portato a definire un modello semplice ma efficace.

\subsubsection{Il modello}
I tempi vengono calcolati per ogni settore indipendentemente dagli altri. E' necessario solamente conoscere la velocità di ingresso nel settore. Ogni settore ha dei parametri che influenzano il tempo di percorrenza: lunghezza, difficoltà della curva, condizioni meteo.
Per calcolare il tempo di percorrenza di un settore, e la velocità di uscita di un auto è necessario:
\begin{itemize}
\item calcolare la velocità massima a cui è possibile percorrere il settore, basandosi sulla difficoltà della curva;
\item calcolare l'accelerazione massima che può avere l'auto nel settore, in dipendenza da: peso dell'auto (quantità di carburante), usura delle gomme, tipo di gomme e clima atmosferico;
\item si ipotizza che l'auto percorra una parte di settore in moto uniformemente accelerato, fino a raggiungere la velocità massima consentita;
\item segue la percorrenza dello spazio rimasto in moto uniforme.
NOTA: se la velocità di ingresso nel settore è maggiore della velocità massima consentita, tutto il settore viene percorso in moto uniforme alla velocità massima.
\end{itemize}

Questo modello non prevede quindi la ``frenata''. Le auto infatti, arrivate ad una curva dopo un rettilineo, riducono istantaneamente la loro velocità. Questa semplificazione non porta però ad incoerenze, perché non considerare i tempi di frenata equivale a considerare tali tempi uguali per tutte le auto, e questa assunzione può essere accettata all'interno di un prototipo come quello in esame. L'applicazione di una simulazione per la frenata, avrebbe portato ad una complessità eccessiva di tutta la gestione della gara, prevedendo anche incidenti ed uscite di pista, esulando dalle tematiche principali di concorrenza e distribuzione. \\

In seguito ai miglioramenti richiesti dal docente, anche le modalità con cui sono calcolati i consumi dipendono dall'evoluzione della competizione. Essi non sono quindi costanti ma dipendono dal peso e dalle velocità raggiunte dall'auto nella corsa.

\section{Car}
Il componente \textit{Car} è la postazione di controllo di un auto. E' assimilabile al ``muretto'' dei box, cioè la postazione dalla quale vengono prese le decisioni strategiche riguardo la corsa dell'auto e monitorati i parametri tecnici e di prestazione.

Per una singola macchina è possibile impostare:
\begin{itemize}
	\item \textbf{gomme}: è possibile scegliere tra due tipologie, \textit{Slick} o gomme da bagnato. Modificabile nel corso della gara.
	\item \textbf{prestazioni}: Impostabili solo all'inizio della competizione. E' possibile assegnare una valore compreso tra 1 e 100. Maggiore è il valore, maggiore è l'accelerazione di cui dispone l'auto. Questo valore è da intendersi come indice di prestazione totale dell'accoppiata \textbf{macchina e pilota}. Non si è scelto di distinguere questi due parametri dato che si sarebbero combinati linearmente ottenendo comunque un unico coefficiente.
	\item \textbf{carburante}: La quota di carburante con cui iniziare la competizione. La massima quantità che l'auto può contenere è impostata a 150 litri.
\end{itemize}
Tramite l'interfaccia è inoltre possibile stabilire dei valori di soglia da considerare per la sosta ai box. Quando i valori relativi al carburante o al consumo dei pneumatici scendono sotto la soglia impostata, viene richiesto automaticamente un \textit{pit stop}.
Per la gestione dei box è possibile impostare il numero di litri di carburante da caricare e il tipo di gomma da montare. In ogni momento della competizione si può forzare la sosta ai box, che verrà effettuata naturalmente alla prima percorrenza del settore idoneo.
Durante la competizione se il carburante termina (0 litri) o i pneumatici si usurano eccessivamente (valore 1), la macchina viene automaticamente ritirata dalla gara. In ogni momento si può forzare il ritiro dal circuito, il quale avviene prima di percorrere il tratto successivo.
Nel corso della competizione le informazioni relative all'auto vengono aggiornate ad ogni settore percorso modificando gli indicatori di carburante ed usura gomme; maggiori dettagli sulla velocità dell'auto vengono proposti in forma testuale. \\
\textit{Car} rappresenta quindi un centro di controllo per l'auto e non la macchina che effettivamente esegue la simulazione la quale, come detto in precedenza, è rappresentata da un task nel componente \textit{Track}.

\subsection{Esecuzione}
Inizialmente l'auto viene iscritta alla gara, ricevendo un \textit{ID} univoco e progressivo in base all'ordine di registrazione. Ottenuto il riferimento remoto del circuito da utilizzare, viene invocata la procedura \textit{PutOnPitLane}, che virtualmente posiziona l'auto nella griglia di partenza. L'ordine di partenza può essere lo stesso dell'ordine di registrazione o casuale, in base alla configurazione. \\
Da questo momento viene instaurata una comunicazione con il task che esegue \textit{PutOnPitLane} in entrambe le direzioni. Il task che partecipa alla simulazione comunica in modo \textit{asincrono} con la propria interfaccia di controllo, questo per non influire nei tempi di percorrenza. Viene comunicata la percorrenza di ciascun settore, la velocità di punta raggiunta e i parametri tecnici aggiornati. \\ L'interfaccia può invece comunicare tramite la \textit{Remote Call Interface} di \textit{Track} la necessità di fermarsi ai box o di ritirarsi dalla competizione. La valutazione di questi valori viene effettuata nel settore precedente ai box o, nel caso del ritiro, prima di percorrere ciascun settore. \\
La conclusione della simulazione avverrà con la terminazione della procedura remota \textit{PutOnPitLane}, indipendentemente dalla motivazione (ritiro o conclusione effettiva).

\section{Race}
\label{Progettazione:Race}
La componente \textit{race} e la relativa interfaccia grafica svolgono la funzione di configurazione, avvio e controllo della competizione. Si può istanziare una singola gara per simulazione. \\
Nella fase iniziale si configurano i parametri principali della simulazione, ovvero:
\begin{itemize}
	\item \textbf{numero di giri},
	\item \textbf{numero di concorrenti massimo},
	\item \textbf{modalità della griglia di partenza}.
\end{itemize}
Il numero dei concorrenti può essere espresso solo come massimo, in quanto il valore effettivo dipende dal numero di nodi (locali o remoti) di \textit{Car} che si notificano durante la fase di registrazione. La fase di iscrizione alla competizione può essere iniziata mediante l'interfaccia grafica, rendendo pubblica la competizione. Durante questa fase viene conteggiato in tempo reale il numero di concorrenti collegati. La registrazione si chiude con l'avvio della gara, che avvia la procedura di partenza del circuito. L'ordine di partenza può avvenire sulla base dell' \textit{ID} di registrazione (completamente deterministico) o su base casuale. \\
Durante lo svolgimento della gara, l'interfaccia fornisce informazioni di debug riguardo la percorrenza dei settori. Per disporre delle informazioni riguardo le posizioni di ciascun concorrente, è possibile istanziare una componente di tipo \textit{Monitor}. \\
La gara svolge inoltre il compito di controllo della terminazione delle auto alla fine dei giri impostati. Attraverso questo componente vengono anche gestite le richieste di terminazione anticipate da parte dei concorrenti.

\section{Monitor}
\label{Progettazione:Monitor}
La funzione della componente \textit{Monitor} è quella di fornire le statistiche aggiornate sullo svolgimento della competizione, a differenza dei parametri tecnici forniti da \textit{Car}. \\
Lo scopo è perseguito presentando due classifiche aggiornate in tempo in reale. \\
Una lista presenta tutti i concorrenti in ordine di posizione, fornendo indicazione sul giro che sta percorrendo e l'ultimo intertempo attraversato. Questo dà la possibilità di visualizzare facilmente lo stato globale della corsa ed altre informazioni ad esempio i sorpassi avvenuti o le macchine doppiate. \\
I valori associati a ciascun pilota nel primo elenco riguardano:
\begin{itemize}
	\item posizione in classifica del concorrente;
	\item \textit{ID} di registrazione dell'auto;
	\item stringa alfanumerica associata al pilota;
	\item stato del concorrente, il quale può essere: in corsa (\textit{running}), ritirato (\textit{retired}), in sosta ai box (\textit{BOX}) o giunto a fine gara (\textit{finished});
	\item numero di soste ai box effettuate;
	\item miglior tempo sul giro ottenuto dal singolo concorrente;
	\item tempo ottenuto sull'ultimo giro percorso.
\end{itemize}

Un'altra lista invece viene utilizzata per fornire i dettagli sui tempi di distacco. Per poter visualizzare coerentemente i dati, è necessario ``fissare'' un punto di controllo, espresso in termini di giro ed intertempo. Fissati questi parametri, le informazioni sono integrate non appena disponibili, ovvero non appena il concorrente attraversa il punto di controllo. Questa funzionalità si è resa necessaria soprattutto per monitorare i concorrenti più lenti. Il tempo indicato per il primo concorrente che attraversa il punto di controllo è anche il tempo di gara in quel determinato istante. Il tempo di gara degli altri concorrenti può essere ottenuto sommando i distacchi.\\
In ogni momento l'interfaccia fornisce informazioni statistiche riguardanti il miglior giro ottenuto e la velocità di punta raggiunta. Al termine della simulazione, un pop-up notifica all'utente la conclusione.\\
A differenza delle altre componenti, il \textit{Monitor} può essere avviato in ogni momento della simulazione e in più istanze. Questo dà la possibilità di ottenere le informazioni ove fosse necessario (ad esempio un concorrente che esegue su un nodo remoto).
