with Simulator;

package Simulator.Controller is
   pragma Remote_Call_Interface;

   type Pool_Access is access all Simulator.Pool_Type'Class;
   type Race_Access is access all Simulator.Remote_Race'Class;
   type Monitor_Access is access all Simulator.Monitor_Pool_Type'Class;

   type RaceId_T is new Positive range 1 .. 1;

   --  a remote access-to-class-wide type used to construct
   --+ dynamically-bound dispatching calls (which are all those
   --+ that have a controlling parameter of a type in the derivation
   --+ hierarchy of this type)
   pragma Asynchronous (Pool_Access);
   pragma Asynchronous (Race_Access);
   --+ when we apply pragma Asynchronous to Pool_Access
   --+ all subprograms that dispatch on Pool_Type'Class and have only
   --+ 'in'-mode parameters become asynchronous RPC

   --  a dynamically-bound dispatching subprogram
   function RegisterCar
     (Pool : Pool_Access;
      CarId  : CarId_T)
      return Boolean;

   function  GetCar
     (CarId : CarId_T)
      return Pool_Access;

   function RegisterRace
     (Race : Race_Access)
      return Boolean;

   function  GetRace
     return Race_Access;

   function SetStop(i: Boolean) return Boolean;

   -- Funzioni di registrazione Monitor
   function RegisterMonitor
     (Monitor : Monitor_Access)
      return Boolean;

   function UnregisterMonitor
     (Monitor : Monitor_Access)
      return Boolean;

   function GetMonitors
     return Natural;

   function  GetMonitor (MonitorId : Positive)
                         return Monitor_Access;

end Simulator.Controller;
