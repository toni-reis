with Ada.Containers.Vectors;
package Simulator.TrackData is
   type Exit_Count_T is new Positive Range 1 .. 1_1_000;
   Package CarIdVector_T is new Ada.Containers.Vectors(Element_Type => CarId_T, Index_Type => Exit_Count_T);
   function "=" (Left, Right : CarIDVector_T.Vector) return Boolean ;

   type Sector_Count_T is new Positive range 1 .. 1_000;
   type ExitOrder_T is array (Positive range <>) of access CarIdVector_T.Vector;

   ExitOrder: access ExitOrder_T ;

end Simulator.TrackData;
