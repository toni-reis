with System;

package Simulator.Car is
   pragma Remote_Types;

   type Car_Type is new Simulator.Pool_Type with record
      CarProperties : CarProperties_T;
      end record;

-- funzione che inizializza l'auto con i suoi parametri e la registra alla gara ed al name server
   function Init(Tires: Tires_T;
                  CarWeight: CarWeight_T;
                  CarFuel: CarFuel_T;
                 CarPerformance: CarPerformance_T;
                Name : String) return Boolean;

   -- aggiorna le caratteristiche dell'auto. � un metodo asincrono
   -- che viene invocato dalla gara ad ogni tratto di pista
   procedure UpdateProperties(This : access Car_Type;
                              CarId: CarId_T;
                              Tires: Tires_T;
                              TiresConsumption: TiresConsumption_T;
                              CarFuel: CarFuel_T;
                              CarPerformance: CarPerformance_T;
                              Lap: Integer;
                             Sector : Natural;
       			     Speed : Float;
       			    SectorDuration : Duration) ;
   procedure Kill;
   procedure CallForPitStop(CarFuel :CarFuel_T; Tires :Tires_T);

end Simulator.Car;
