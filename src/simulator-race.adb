with Ada.Text_IO; use Ada.Text_IO;
with Simulator.Controller;
with Simulator.Track;
with Ada.Calendar; use Ada.Calendar;
with Simulator.Statistics; use Simulator.Statistics;

with Simulator.Race.Gui; use Simulator.Race.Gui;
pragma Elaborate(Simulator.Race.Gui);

package body Simulator.Race is
   n_registered :Natural := 0;
   Touch : Boolean;
   Gara : aliased The_Race;

   function StartRace(Randomized : Boolean) return Boolean is
      i: Boolean;
      k : Boolean;
   begin
      -- la corsa parte solo se n_registered � nel range CarId_T
      if (n_registered < Natural(CarId_T'First) or else n_registered > Natural(CarId_T'Last)) then
         return False;
      else
      k := Simulator.Track.StartRace(CarId_T(n_registered),Randomized);
      if (k = True) then
         Gara.SetStarted(True);
         i :=Simulator.Controller.SetStop(True);
      end if;
         return k;
         end if;
   end StartRace;

   procedure Init(maxCars: CarId_T; laps: Integer) is
   begin
      Gara.SetParameters(maxCars, laps);
      Touch := Simulator.Controller.RegisterRace (Gara'Access);
   end Init;

   type LapDuration_T is array (CarId_T) of Duration;

   task body The_Race is
      CarsNumber: CarId_T;
      LapsNumber : Integer;
      pos : Integer;
      BestLap : Duration := Duration'Last;
      LastLaps : LapDuration_T;
      BestLaps : LapDuration_T;
      MaxSpeed : Float := 0.0;
      LapDuration : LapDuration_T;
      Started : Boolean := False;
      OldLap : Natural := 0;
      OldWakeUp : Duration := 0.0;
      OldElapsedTime : Duration := 0.0;
      ElapsedTime : Duration := 0.0;
      StartTime : Duration := 0.0;
      Terminated : RetireRequest_T;
      RaceEnded : Boolean := False;

   begin
      for i in CarId_T loop
         LapDuration(i) := 0.0;
         BestLaps(i) := Duration'Last;
         LastLaps(i) := Duration'Last;
      end loop;
      loop
         select
            accept SetStarted(start: Boolean) do
               Started := start;
            end SetStarted;
         or  accept SetParameters(maxCars: CarId_T; laps: Integer) do
               CarsNumber := maxCars;
               LapsNumber := laps;
               for i in CarId_T loop
                  Terminated(i) := False;
               end loop;
               Print_NoLock ("DEBUG: Gara inizializzata, "& laps'Img &" giri, "& maxCars'Img &" auto massimo.");
            end SetParameters;
         or accept CarRegister (id: out CarId_T; Success: out Boolean)  do
               if(n_registered < Natural(CarsNumber) and then Started = False) then
                  n_registered := n_registered +1;
                  id := CarId_T(n_registered);
                  Success := True;
                  UpdateRegistered(n_registered);
               else
                  id := CarsNumber;
                  Success := False;
               end if;
            end CarRegister;

         or    accept  Kill(CarId: in CarId_T) do
               Track.Kill(CarId => CarId);
               Print("Entry Kill eseguita sull'auto:"&CarId'Img);
               --   se la corsa non era gia finita, notifico ai monitor il ritiro
               if (Terminated(CarId) = False) then
                  for I in 1 .. Simulator.Controller.GetMonitors loop
                     Simulator.Controller.GetMonitor(I).UpdateCarStatus(CarId, CarStatus_T(Retired));
                  end loop;
               end if;
               Terminated(CarId) := True;
               if (RaceEnded = False) then
                  RaceEnded := True;
                  for i in 1 .. n_registered loop
                     if ( Terminated(CarId_T(i)) = False) then
                        RaceEnded := False;
                     end if;
                  end loop;
                  if (RaceEnded = True) then
                     Print("Gara terminata");
                     for I in 1 .. Simulator.Controller.GetMonitors loop
                        Simulator.Controller.GetMonitor(I).EndRace;
                        --  Print("Intertempo "&Intermediate'Img&": aggiorno monitor");
                     end loop;
                  end if;
               end if;
            end Kill;

         or accept UpdateStatistics(Id : CarId_T; Name : String; Lap : Integer; Sector : Integer; WakeupTime : Duration; Speed : Float; Intermediate: Integer) do
               --Print(" Aggiorno statistiche Lap:"&Lap'Img&" Sector:"&Sector'Img);
               -- DROP dei pacchetti provenienti da auto che hanno gia terminato la gara
               if (Terminated(CarId_T(Id)) = False) then
                  if (Lap /= 0)then
                     Ranking.GetLap(Id,OldLap);
                     if (Lap > OldLap  and then Lap <= LapsNumber + 1 and then OldLap /= 0) then
                        LastLaps(Id) := LapDuration(Id);
                        if (BestLaps(Id) > LastLaps(Id)) then
                           BestLaps(Id) := LastLaps(Id);
                        end if;
                        Put_Line("Ultimo giro dell'auto "&Id'Img&" durato "&LastLaps(Id)'Img);
                        Put_Line("Miglior giro dell'auto "&Id'Img&" durato "&BestLaps(Id)'Img);
                        if ( BestLap > LapDuration(Id)) then
                           BestLap := LapDuration(Id);
                           for I in 1 .. Simulator.Controller.GetMonitors loop
                              Simulator.Controller.GetMonitor(I).Update_BestLap(Id,Lap - 1,BestLap);
                              Print("Intertempo "&Intermediate'Img&": aggiorno monitor");
                           end loop;

                           Print("Nuovo tempo migliore sul giro "&BestLap'Img);
                        else
                           null;
                           --    Print("Tempo sul giro non migliorante "&LapDuration(Id)'Img);
                        end if;
                        -- comunico nuovi tempi sul giro a monitor
                        for I in 1 .. Simulator.Controller.GetMonitors loop
                           Simulator.Controller.GetMonitor(I).UpdateLapTime(Id,LastLaps(Id),BestLaps(Id));
                           Print("Intertempo "&Intermediate'Img&": aggiorno monitor");
                        end loop;
                        LapDuration(Id) := 0.0;
                     end if;

                     if (Lap = 1 and then Sector = 1) then
                        OldWakeUp := StartTime;
                     else
                        Ranking.GetWakeUpTime(Id,OldWakeUp);
                     end if;

                     LapDuration(Id) := LapDuration(Id) + WakeupTime - OldWakeUp;
                     Ranking.GetElapsedTime(Id,OldElapsedTime);
                     ElapsedTime := OldElapsedTime + WakeupTime - OldWakeUp;
                  end if;
                  if (Lap <= LapsNumber) then
                     -- il primo tempo che arriva � il tempo di inizio della gara, se poi arriva un tempo
                     -- pi� basso tengo quello come inizio( inversione pacchetti)
                     if (StartTime = 0.0 or WakeUpTime < StartTime) then
                        StartTime := WakeupTime;
                     end if;
                     Ranking.Insert(item => (Id,Lap,Sector, ElapsedTime, WakeupTime),
                                    pos  => pos);
                     if (Speed > MaxSpeed) then
                        MaxSpeed := Speed;
                        for I in 1 .. Simulator.Controller.GetMonitors loop
                           Simulator.Controller.GetMonitor(I).Update_MaxSpeed(Id,Speed);
                           Print("Nuova velocita' massima :"&MaxSpeed'Img&": aggiorno monitor");
                        end loop;
                     end if;

                     if (Intermediate /= 0) then
                        for I in 1 .. Simulator.Controller.GetMonitors loop
                           Simulator.Controller.GetMonitor(I).UpdateMonitor(pos,Id,Name,Lap,Intermediate,WakeUpTime,Speed,ElapsedTime);
                        end loop;
                     end if;


                  else
                     --La corsa � terminata per l'auto attuale
                     Track.Kill(CarId => Id);
                     Terminated(Id) := True;
                     for I in 1 .. Simulator.Controller.GetMonitors loop
                        Simulator.Controller.GetMonitor(I).UpdateCarStatus(Id, CarStatus_T(Finished));
                     end loop;

                     if (RaceEnded = False) then
                        Print("Terminata la corsa per l'auto:"&Id'Img);
                        RaceEnded := True;
                        for i in 1 .. n_registered loop
                           if ( Terminated(CarId_T(i)) = False) then
                              RaceEnded := False;
                              -- Print("Auto "&i'Img&" ancora in gara");
                           end if;
                        end loop;
                        if (RaceEnded = True) then
                           Print("Gara  terminata");
                           for I in 1 .. Simulator.Controller.GetMonitors loop
                              Simulator.Controller.GetMonitor(I).EndRace;
                              --  Print("Intertempo "&Intermediate'Img&": aggiorno monitor");
                           end loop;
                        end if;
                     end if;
                  end if;
               end if;
            end UpdateStatistics;
         or accept  UpdateCarStatus(Id : CarId_T; Lap: Integer; Status : CarStatus_T) do
               if (Terminated(Id) = False and then Lap <= LapsNumber ) then
                  -- L'auto � ai box. Notifico il fatto ai monitor.
                  for I in 1 .. Simulator.Controller.GetMonitors loop
                     Simulator.Controller.GetMonitor(I).UpdateCarStatus(Id, CarStatus_T(Box));
                  end loop;
               end if;

            end UpdateCarStatus;

         or terminate;
         end select;

      end loop;

   end The_Race;

end Simulator.Race;
