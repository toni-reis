package body Simulator.Statistics is

   protected body Ranking is

      procedure Insert(item : RankingLine_T; pos:out Natural) is
         i : Integer := 1;
      begin
         Delete(item);
         for Index in 1 .. N loop
            if ((R(i).Lap > item.Lap) or else
                  (R(i).Lap = item.Lap and R(i).Sector > item.Sector ) or else
                  (R(i).Lap = item.Lap and R(i).Sector = item.Sector and R(i).WakeupTime < item.WakeupTime) ) then
               i := i + 1;
            else exit;
            end if;
         end loop;
         -- i indica la posizione nell'array in cui deve essere inserito Item
         -- sposto tutti gli oggetti dalla posizione Index alla posizione N indietro di 1 posizione
         for S in reverse i .. N loop
            R(S+1) := R(S);
         end loop;
         -- aggiorno la dimensione dell'array e inserisco il record in posizione Index
         R(i) := item;
         N := N+1;
         pos := i;
      end Insert;

      procedure Search(id : in CarId_T; i : out Natural) is
      begin
         i := 0;
         for Index in 1 .. N loop
            if (R(Index).Id = id) then
               i := Index;
            end if;
         end loop;
      end Search;


      procedure Delete(item : RankingLine_T) is
         i: Natural := 0;
      begin
         Ranking.Search(item.Id,i);
         -- se l'elemento � presente sposto indietro di 1 tutti gli elementi dell'array da i+1 a N e decremento N
         if( i /= 0) then
            for Index in i .. N-1 loop
               R(Index) := R(Index+1);
            end loop;
            N := N-1;
         end if;
      end Delete;

      procedure Clear is
      begin
         N := 0;
      end Clear;

      procedure GetWakeUpTime(id : in CarId_T; wakeUp :out Duration) is
         i : Integer := 0;
      begin
         Search(id, i);
         wakeUp:= R(i).WakeupTime;
      end GetWakeUpTime;

      procedure GetElapsedTime(id : in CarId_T; elapsed :out Duration) is
         i : Integer := 0;
      begin
         Search(id, i);
         elapsed:= R(i).ElapsedTime;
      end GetElapsedTime;

      procedure GetLap(id : in CarId_T; lap :out Natural) is
         i : Integer := 0;
      begin
         Search(id, i);
         lap:= R(i).Lap;
      end GetLap;

   end Ranking;

end Simulator.Statistics;
