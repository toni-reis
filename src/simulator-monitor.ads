package Simulator.Monitor is
   pragma Remote_Types;

   type Monitor_Type is new Simulator.Monitor_Pool_Type with null record;

   procedure Init;
   procedure Quit;

   procedure UpdateMonitor
     (This : access Monitor_Type;
      Pos: Positive;
      CarId: CarId_T;
      Name : String;
      Lap: Natural;
      Intermediate: Natural;
      WakeupTime: Duration;
      Vel: Float;
      TimeElapsed: Duration);
   procedure UpdateLapTime (This : access Monitor_Type; Id: CarId_T;  LastLap : Duration; BestLap: Duration);
   procedure UpdateCarStatus (This : access Monitor_Type; Id: CarId_T;  Status : CarStatus_T);
   procedure Update_BestLap(This : access Monitor_Type; Id : CarId_T; Lap : Positive; Time : Duration);
   procedure Update_MaxSpeed(This : access Monitor_Type; Id : CarId_T; Val : Float);
   procedure EndRace(This : access Monitor_Type);

end Simulator.Monitor;
