-- include GTK
with Gdk.Event; use Gdk.Event;
with Gtk.Widget; use Gtk.Widget;
with Gtk.Handlers; use Gtk.Handlers;

with Gtk.Window; use Gtk.Window;
with Gtk.Label; use Gtk.Label;
with Gtk.GEntry; use Gtk.GEntry;
with Gtk.Combo_Box; use Gtk.Combo_Box;
with Gtk.Spin_Button; use Gtk.Spin_Button;
with Gtk.Scale; use Gtk.Scale;
with Gtk.Button; use Gtk.Button;
with Gtk.Text_View; Use Gtk.Text_View;

package Simulator.Car.Gui is
   -- elementi GUI usati da piu' procedure
   Window : Gtk_Window;
   ValId : Gtk_Label;
   ValName : Gtk_Entry;
   ValTyres : Gtk_Combo_Box;
   ValPerformance : Gtk_HScale;
   ValFuel : Gtk_HScale;
   ValFuelLimit : Gtk_HScale;
   ValTyresConsum : Gtk_HScale;
   ValTyresConsumLimit : Gtk_HScale;
   Button : Gtk_Button;
   Console : Gtk_Text_View;

   ValTyresBox : Gtk_Combo_Box;
   ValFuelBox : Gtk_Spin_Button;
   ButtBox : Gtk_Button;
   LabelUsage : Gtk_Label;
   ButtKill : Gtk_Button;

   procedure Init_Car_Gui;
   procedure Print(str : String);
   procedure Print_NoLock(str : String);
   procedure SetWinTitle(str : String);
   procedure Start_Car (Widget : access Gtk_Widget_Record'Class);
   procedure PitStop (Widget : access Gtk_Widget_Record'Class);
   procedure SetUsage(fuel : Float; tyres : Float);
   procedure Retire_Car (Widget : access Gtk_Widget_Record'Class);
end Simulator.Car.Gui;
