with Ada.Calendar; use Ada.Calendar;
package Simulator.Statistics is

   type RankingLine_T is record
      Id : CarId_T;
      Lap : Natural;
      Sector : Natural;
      ElapsedTime : Duration; -- tempo di gara trascorso
      WakeupTime : Duration; -- tempo dell'ultimo risveglio
   end record;
   type RankingArray_T is array (Integer range 1 .. MaxId) of RankingLine_T;

   protected Ranking is
      procedure Insert( item : in RankingLine_T; pos:out Natural);
      procedure Clear;
      procedure GetWakeUpTime(id : in CarId_T; wakeUp :out Duration);
      procedure GetElapsedTime(id : in CarId_T; elapsed :out Duration);
      procedure GetLap(id : in CarId_T; lap :out Natural);
   private
      procedure Delete (item : in RankingLine_T);
      procedure Search(id : in CarId_T; i : out Natural);
      R : RankingArray_T;
      N : Integer := 0;
   end Ranking;

end Simulator.Statistics;
