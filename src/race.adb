with Ada.Text_IO; use Ada.Text_IO;
with Simulator; use Simulator;
with Ada.Command_Line; use Ada.Command_Line;
with Simulator.Race.Gui; use Simulator.Race.Gui;

procedure Race is
   n : Natural;
   n_cars_int : Integer;
   n_cars : CarId_T := 10;
begin
   -- controllo se � stato passato il numero di auto come primo parametro.
   begin
      n := Argument_Count;
      --  Put_Line("Race: DEBUG passati "&n'Img&" parametri");
      if (n > 0) then
         n_cars_int := Integer'Value(Argument(1));
         Put_Line("Race: numero massimo di auto "&n_cars_int'Img);
         if (n_cars_int >= Integer(CarId_T'First) and then n_cars_int <= Integer(CarId_T'Last)) then
            n_cars := CarId_T(n_cars_int);
         end if;
      end if;
   exception when Others =>
         n_cars := 10;
         Put_Line("Race: il parametro passato non indica un numero di auto valido");
   end;
   -- n_cars contiene il numero di auto passato come parametro, se ce n'� uno , oppure 10.
   Init_Race_Gui(n_cars);
end Race;
