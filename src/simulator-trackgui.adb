with Ada.Text_IO; use Ada.Text_IO;
with Simulator.Track; use Simulator.Track;
with Simulator.Gui; use Simulator.Gui;

-- include GTK
with Gdk.Threads;
with Gtk.Main;
with Gtk.Widget;
with Gtk.Window; use Gtk.Window;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;
with Gtk.Enums; use Gtk.Enums;
with Gtk.Alignment; use Gtk.Alignment;
with Gtk.Box; use Gtk.Box;
with Gtk.Table; use Gtk.Table;
with Gtk.Text_Buffer; Use Gtk.Text_Buffer;
with Gtk.Text_Iter; use Gtk.Text_Iter;
with Gtk.Text_Mark; use Gtk.Text_Mark;

package body Simulator.TrackGui is
   procedure Init_Track_Gui is
      Window : Gtk_Window;
      VBox : Gtk_Box;
      Table : Gtk_Table;
      align : Gtk_Alignment;
      Label : Gtk_Label;
      Scroller : Gtk_Scrolled_Window;
   begin
      -- Inizializzo Track
      Simulator.Track.InitTrack;
      -- Inizializzo GTK e la finestra principale
      Gdk.Threads.G_Init;
      Gdk.Threads.Init;
      Gtk.Main.Init;
      Gtk.Window.Gtk_New (Window);
      -- Bordo della finestra e bottone centrale
      Gtk.Window.Set_Border_Width (Window, 5);
      Set_Size_Request (Window, 400, 250);

      Gtk_New_Vbox (VBox, False, 5);
      Gtk_New(Table, 2, 2, False);
      --- intestazione centrale ---
      Gtk_New(Label, "File di impostazione tracciato:");
      Gtk_New(align, 0.5, 0.5, 0.0, 0.0);
      Add(align, Label);
      Attach(Table,align, 0, 2, 0, 1, Fill, Fill, 5, 5);

      --- Bottone selezione file ---
      Gtk_New(ButtonFile, "Choose track config file.", Gtk.File_Chooser.Action_Open);
      Set_Width_Chars(ButtonFile, 35);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ButtonFile);
      Attach(Table,align, 0, 1, 1, 2, Fill, Fill, 5, 5);

      --- Bottone Carica ----
      Gtk_New(ButtonLoad, "Load");
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, ButtonLoad);
      Attach(Table,align, 1, 2, 1, 2, Fill, Fill, 5, 5);

      Gtk_New (Scroller);
      Set_Policy  (Scroller, Policy_Automatic, Policy_Always);
      Gtk_New (Console);
      Set_Editable (Console, False);

      -- impacchetto nel layout
      Pack_Start(VBox, Table, False, True, 5);
      Add(Scroller, Console);
      Pack_Start(VBox, Scroller, True, True, 5);

      Add(Window, VBox);

      -- Connessione segnali
      -- collego il segnale di chiusura del Window Manager alla funzione Delete_Event
      Return_Handlers.Connect(Window, "delete_event", Return_Handlers.To_Marshaller (Delete_Event'Access));
      --  callback chiamato quando si invoca "destroy" su Window, o quando
      --  la funzione Delete_Event ritorna false
      Handlers.Connect(Window, "destroy", Handlers.To_Marshaller (Destroy'Access));
      -- callback del tasto load
      Handlers.Connect(ButtonLoad, "clicked", Handlers.To_Marshaller (Load_Config'Access));

      -- Rendo tutto visibile
      Show_All (Window);
      Print("GUI: Interfaccia utente circuito inizializzata.");

      -- Metto in attesa l'applicazione nel loop GTK
      Gdk.Threads.Enter;
      Gtk.Main.Main;
      Gdk.Threads.Leave;
      Put_Line ("GUI: terminato Gtk.Main.Main di circuito. Termino l'applicazione.");
   end Init_Track_Gui;

   procedure Print(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Gdk.Threads.Enter;
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
      Gdk.Threads.Leave;
   end Print;

   procedure Print_NoLock(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
   end Print_NoLock;

   -- Callback per il caricamento del file
   procedure Load_Config (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin
      Print_NoLock ("DEBUG: Carico File: " & Get_Filename(+(ButtonFile)));
      if (ReadTrackConf(Get_Filename(+(ButtonFile)))) then
      Set_Sensitive(ButtonFile, False);
         Set_Sensitive(ButtonLoad, False);
         end if;
   end Load_Config;

end Simulator.TrackGui;
