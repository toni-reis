with Ada.Text_IO; use Ada.Text_IO;
with Gtk.Main;
with Glib; use Glib;

package body Simulator.Gui is

   function Delete_Event
     (Widget : access Gtk_Widget_Record'Class;
      Event  : Gdk_Event)
     return Boolean
   is
      pragma Unreferenced (Event);
      pragma Unreferenced (Widget);
   begin
      --  If you return False in the "delete_event" signal handler,
      --  GtkAda will emit the "destroy" signal. Returning True means
      --  you don't want the window to be destroyed.
     Gtk.Main.Main_Quit;

      Put_Line ("GUI: delete event occurred");
      return False;
   end Delete_Event;

   --   Another callback
   procedure Destroy (Widget : access Gtk_Widget_Record'Class) is
      pragma Unreferenced (Widget);
   begin
      null;
      Gtk.Main.Gtk_Exit(0);
   end Destroy;

end Simulator.Gui;
