with Ada.Numerics;
with Ada.Numerics.Generic_Elementary_Functions;

with Ada.Containers; use Ada.Containers;
package body Simulator.TrackData is
   function "=" (Left, Right : CarIDVector_T.Vector) return Boolean is
      length : Count_Type := Left.Length;
      equals : Boolean := True;
   begin
      if (Right.Length /= length) then
         return False;
      end if;
      for i in Count_Type(Exit_Count_T'First) .. length loop
         if Left.Element(Exit_Count_T(i)) /= Right.Element(Exit_Count_T(i)) then
            return False;
         end if;
      end loop;
      return True;
   end "=";



end Simulator.TrackData;
