with System;
with Simulator.Car;

package Simulator.Track is
   pragma Remote_Call_Interface;

   --procedure che legge il file di configurazione e crea le strutture dati adeguate
   function ReadTrackConf(ConfFile : String) return Boolean;
   procedure InitTrack;

   -- inserisce la macchina nel circuito, fornisce in ingresso le
   -- caratteristiche iniziali dell'auto. Quando questo metodo termina vuol
   -- dire che la macchina ha finito la gara (conclusa o per ritiro)
   procedure PutOnPitLane(my_CarProperties: CarProperties_T);

   -- metodo invocato remotamente dal controllo gara.
   -- Fa partire la simulazione effettiva della corsa
   function StartRace(n_cars : CarId_T; Randomized: Boolean) return Boolean;
   --pragma Asynchronous (StartRace);

   -- Comunica di ritirare l'auto con id = CarId dal circuito
   procedure Kill(CarId: in CarId_T);
   pragma Asynchronous (Kill);
   -- metodo di richiesta fermata ai box
   -- l'invocazione di questo metodo porta il circuito a far fare una sosta
   -- all'auto "CarId" appena possibile.
   procedure CallForPitStop(CarId: in CarId_T;CarFuel :CarFuel_T; Tires :Tires_T);

end Simulator.Track;
