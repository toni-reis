with Ada.Text_IO; use Ada.Text_IO;
with Simulator.Car;
with Simulator; use Simulator;

with Simulator.Car.Gui; use Simulator.Car.Gui;

procedure Car  is
begin
   Init_Car_Gui;
end Car;
