with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;

with Simulator.Controller;
with Simulator.Monitor.Gui;

package body Simulator.Monitor is
   package DurationIO is new Ada.Text_IO.Fixed_Io(Duration);
   use Simulator;

   Own_Monitor : aliased Monitor_Type;

   procedure Init is
      b : Boolean;
   begin
      b := Simulator.Controller.RegisterMonitor (Own_Monitor'Access);
      Put_Line("Monitor registrato: " & b'Img & ".");
   end Init;

   procedure Quit is
      b : Boolean;
   begin
      b := Simulator.Controller.UnregisterMonitor (Own_Monitor'Access);
      Put_Line("Monitor deregistrato: " & b'Img & ".");
   end Quit;

   procedure UpdateMonitor
     (This    : access Monitor_Type;
      Pos: Positive;
      CarId: CarId_T;
      Name : String;
      Lap: Natural;
      Intermediate: Natural;
      WakeupTime: Duration;
      Vel: Float;
      TimeElapsed: Duration) is
   begin
      Simulator.Monitor.Gui.Update_Model(Pos, CarId, Name, Lap, Intermediate, WakeupTime, Vel, TimeElapsed);
   end UpdateMonitor;

   procedure UpdateLapTime (This : access Monitor_Type; Id: CarId_T;  LastLap : Duration; BestLap: Duration) is
   begin
       Simulator.Monitor.Gui.UpdateLapTime ( Id,LastLap, BestLap);
   end UpdateLapTime;

   procedure UpdateCarStatus (This : access Monitor_Type; Id: CarId_T;  Status : CarStatus_T) is
   begin
      Simulator.Monitor.Gui.UpdateCarStatus (Id,Status );
      end UpdateCarStatus;

   procedure Update_BestLap(This : access Monitor_Type; Id : CarId_T; Lap : Positive; Time : Duration) is
   begin
      Simulator.Monitor.Gui.Update_BestLap(Id, Lap, Time);
   end Update_BestLap;

   procedure Update_MaxSpeed(This : access Monitor_Type; Id : CarId_T; Val : Float) is
   begin
      Simulator.Monitor.Gui.Update_MaxSpeed(Id, Val);
   end Update_MaxSpeed;

   procedure EndRace(This : access Monitor_Type) is
   begin
      Simulator.Monitor.Gui.EndRace;
   end EndRace;

end Simulator.Monitor;
