with System;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;
with Ada.Containers.Vectors;
with Ada.Numerics;
with Ada.Numerics.Generic_Elementary_Functions;


with Simulator;
with Simulator.Controller;
with Simulator.Car; use Simulator.Car;
with Simulator.Statistics; use Simulator.Statistics;
with Simulator.TrackGui; use Simulator.TrackGui;
with Simulator.TrackData; use Simulator.TrackData;

with Unicode.CES;

with Schema.Schema_Readers, Schema.Validators, Input_Sources.File;
use  Schema.Schema_Readers, Schema.Validators, Input_Sources.File;
with Schema.Schema_Grammar;  use  Schema.Schema_Grammar;

with Sax.Readers;
with Schema.Readers;     use Schema.Readers;
with Schema.Dom_Readers; use Schema.Dom_Readers;

--with DOM.Readers;        use DOM.Readers;
with DOM.Core;           use DOM.Core;
with DOM.Core.Documents; use DOM.Core.Documents;
with DOM.Core.Nodes;     use DOM.Core.Nodes;
with DOM.Core.Attrs;     use DOM.Core.Attrs;

with Ada.Numerics.Float_Random;
use Ada.Numerics.Float_Random;
with Ada.Exceptions; use Ada.Exceptions;


package body Simulator.Track is
   type Intermediate_Count_T is new Positive range 1 .. 1_000;
   package IntVector is new Ada.Containers.Vectors(Element_Type => Integer, Index_Type => Intermediate_Count_T);
   package Float_Functions is new Ada.Numerics.Generic_Elementary_Functions (Float);



   --STRUTTURE  DATI
   MaxMultiplicity : Positive := 3;
   -- n_registered : Natural :=0;
   BoxSpeedLimit : Float := 22.2; -- m/s
   PitStopSector : Positive;
   Intermediate : IntVector.Vector;
   TrackStarted : Boolean := False;

   type PitStopRequest_T is array (CarId_T) of Boolean ;

   type PitStopRecord_T is record
      CarFuel : CarFuel_T;
      Tires : Tires_T;
   end record;

   type PitStopData_T is array (CarId_T) of PitStopRecord_T;
   PitStopData : PitStopData_T;
   PitStopRequest: PitStopRequest_T;
   type RetireRequest_T is array (CarId_T) of Boolean ;
   RetireRequest: PitStopRequest_T;

   procedure CalculateDriveTime(my_properties : CarProperties_T; my_length: Natural; my_level : Integer; weather: Weather_T; isbox : Boolean; my_fuel:  out Float; my_consumption :  out Float; my_speed : in out Float; finish : out Duration)  is
      Vmax : Float := 100.0;
      difficulty : Integer := my_level;
      -- dati per calcolo velocit� e accelerazione
      a : Float := 9.25;
      Smax : Float:= Float(my_length);
      S : Float;
      V0 : Float := my_speed;
      Vfinal : Float;
      t : Float := 0.0;
   begin
      if (difficulty < 0) then
         difficulty := - difficulty;
      end if;
      a:= a - a*0.8* ( 0.3 * (1.0 - Float(my_properties.CarPerformance) / 100.0) + 0.10 * Float(my_properties.TiresConsumption )  + 0.4*( (Float(difficulty) / Float(MaxLevel))) + 0.20 * ( Float(my_properties.CarFuel)/ Float(MaxFuel)));
      if (weather = Wet) then
         -- se piove diminuiso l'accelerazione massima di 1/3;
         a := a / 3.0;
         if (my_properties.Tires = Slick) then
            -- se piove e monto le gomme da asciutto diminuisco l'accelerazione di un ulteriore terzo
            a := a/3.0;
         end if;
      else
         -- Il tracciato � asciutto
         if (my_properties.Tires = Rain) then
            -- monto le gomme da bagnato
            a := a/3.0;
         end if;
      end if;

      Vmax := Vmax * Float( Float(1 + MaxLevel - difficulty)/ Float(1 + MaxLevel));
      if (isbox) then
         Vmax := Float(BoxSpeedLimit);
      end if;
      if (V0 < Vmax) then

         -- Parte in moto uniformemente accelerato
         S := ((Vmax*Vmax) - (V0*V0))/(2.0*a);

         if (S <= Smax) then
            -- pezzo in moto uniformemente accelerato
            Vfinal := Vmax;
            t := (Vmax - V0)/a;
            -- pezzo in moto rettilineo uniforme
            t := t + ((Smax - S) / Vmax);

         else
            -- percorro tutto il tratto accelerando
            Vfinal := Float_Functions.Sqrt((V0*V0) + (2.0*a*Smax));
            t :=  (Vfinal - V0) / a;
         end if;
      else
         --arrivo a velocit� troppo alta. Freno in maniera istantanea e percorro il tratto
         --a velocit� costante
         t := Smax / Vmax;
         Vfinal := Vmax;
      end if;
      -- Vfinal � la velocit� di uscita dal tratto, t il tempo impiegato
      -- in ogni caso, aggiorno il consumo
      my_consumption := Float(my_length) / 180000.0;
      my_consumption := my_consumption*( 1.0 + 0.2* (Float(my_properties.CarPerformance) / 100.0) + 0.3* Float(my_properties.CarFuel)/ Float(MaxFuel) );
      my_fuel := - Float(my_length) / 2040.0;
      my_fuel := my_fuel*( 1.0 + 0.3* ( Float(my_properties.CarPerformance) / 100.0) + 0.2* Float(my_properties.CarFuel)/ Float(MaxFuel) );
      if (isbox) then
         if ( (Float( PitStopData(my_properties.CarId).CarFuel ) / 12.0) < 5.5) then
            t := t + 5.5;

         else
            t := t + Float(Float(PitStopData(my_properties.CarId).CarFuel) / 12.0);
         end if;
         my_consumption := Float(- my_properties.TiresConsumption);
         my_fuel := Float(PitStopData(my_properties.CarId).CarFuel);
      end if;

      my_speed := Vfinal;

      finish := Duration(t);

   end CalculateDriveTime;

   protected type Semaphore  is
      entry Wait;       -- P
      entry Internal;
      procedure Signal(start : Boolean; car : out Boolean); -- V
   private
      Started : Boolean := false;
      Used : Boolean := False;
   end Semaphore;
   type Semaphore_Ref is access Semaphore;
   protected body Semaphore  is
      entry Wait when True is
      begin
         Used := True;
         requeue Internal;
      end Wait;-- P
      entry Internal when Started = True is
      begin
         null;
      end Internal;
      procedure Signal(start : Boolean; car : out Boolean) is
      begin
         Started := start;
         car := Used;
         Used := False;
      end Signal;

   end Semaphore;


   protected type Lane_T(my_length: Natural; my_level : Integer;my_weather : Weather_T; my_isbox : Boolean) is
      entry Demand(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float);
   private
      Weather : Weather_T :=my_weather;
      IsBox : Boolean := my_isbox;
      exit_time : Time := Clock;
      exit_speed : Float := 0.0;
      Level : Integer := my_level;
      Length : Natural := my_length;
      max : Natural;
      maxId : CarId_T;
      tot : Integer :=0;
      entered : Integer :=0;
      open : Boolean := False;
   end Lane_T;
   type Corsie_T is array (Positive range <>) of access Lane_T;
   type Corsie_Array_T is array (Positive range <>) of access Corsie_T;
   Corsie : access Corsie_Array_T;
   type PitLane_T is array(CarId_T) of Semaphore_Ref;
   PitLane: access PitLane_T := new PitLane_T;

   type LaneCounter_T is array (Positive range<>) of Natural;
   type CarLane_T is array (CarId_T) of Positive;

   type ExitRecord is record
      CarId : CarId_T;
      CarTime : Time := Clock;
      Arrived : Boolean := False;
   end record;
   type ExitArray_T is array (Integer range <>) of ExitRecord;

   protected type Sector(my_sector_id: Natural; my_multiplicity : Natural; my_length: Natural; my_level : Integer; my_isbox: Boolean ) is
      --sceglie la corsia da percorrere, cio� quella con meno traffico, calcola il tempo di percorrenza e effettua la requeue su di essa
      entry Enter(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float);
      entry Release(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float);
      entry BookExit(my_properties: CarProperties_T; my_time : Time);

   private
      entry ExitLane(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float);
      ExitArray :  ExitArray_T(1 .. MaxId);
      ExitCount : Natural := 0;
      LaneCounter : LaneCounter_T(1 .. my_multiplicity) ;
      CarLane : CarLane_T;
      Free : Natural := MaxId;
      MaxCars : Natural := MaxId;
      Sector_Id: Natural :=my_sector_id;
      Length : Natural := my_length;
      Multiplicity: Natural := my_multiplicity;
      Level : Integer := my_level;
      IsBox : Boolean := my_isbox;
      -- per la exit
      Changed : Boolean := False;
      max : Natural;
      tot : Integer :=0;
      entered : Integer :=0;
      open : Boolean := False;
   end Sector;

   type Sector_Array_T is array (Positive range <>) of access Sector;
   -- settori circuito
   Sectors : access Sector_Array_T;
   -- settore box
   BoxSector : access Sector;

   procedure CheckSurpass(Sector_Id : Natural; CarId : CarId_T) is
   begin
      Print("Controllo i sorpassi nel settore"& Sector_Id'Img);
      if (Sector_Id /= 1) then
         if ( (not ExitOrder(Sector_Id - 1).Is_Empty) ) then
            if (ExitOrder(Sector_Id - 1).First_Element /= CarId) then
               Print("Errore : L'auto ha superato tra l'uscita di un settore e l'entrata nel settore "& Sector_Id'Img);
            end if;
            -- cancello il primo elemento
            ExitOrder(Sector_Id - 1).Delete_First;
         end if;
      else
         if ( (not ExitOrder(Sectors'Length).Is_Empty)) then
            if( ExitOrder(Sectors'Length ).First_Element /= CarId) then
               Print("Errore : L'auto ha superato tra l'uscita di un settore e l'entrata nel settore "& Sector_Id'Img);
            end if;
            -- cancello il primo elemento
            ExitOrder(Sectors'Length).Delete_First;
         end if;
      end if;
   end CheckSurpass;


   protected body Sector is
      entry BookExit(my_properties: CarProperties_T; my_time : Time) when True is
      begin
         if (ExitCount <= 0) then
            ExitArray(1) := (my_properties.CarId, my_time, False);
            ExitCount := ExitCount + 1;
         else
            for Index in  reverse  0 .. ExitCount loop
               if(Index = 0 ) then
                  for I in reverse (Index + 1) .. ExitCount loop
                     ExitArray(I+1) := ExitArray(I);
                  end loop;
                  ExitArray(Index + 1) := (my_properties.CarId, my_time, False);
                  ExitCount := ExitCount + 1;
                  exit;
               else  if(ExitArray(Index).CarTime < my_time) then
                     for I in reverse (Index + 1) .. ExitCount loop
                        ExitArray(I+1) := ExitArray(I);
                     end loop;
                     ExitArray(Index + 1) := (my_properties.CarId, my_time, False);
                     ExitCount := ExitCount + 1;
                     exit;
                  end if;
               end if;
            end loop;
         end if;
      end BookExit;

      entry Enter(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float) when Free > 0 is
         n_lane : Natural;
      begin
         --Prima auto a entrare nel tracciato azzera i campi dato
         if (MaxCars = Free) then
            for Index in 1 .. Multiplicity loop
               LaneCounter(Index) := 0;
            end loop;
         end if;
         -- decido la corsia da percorrere. Trovo quella meno trafficata

         n_lane := 1;
         for Index in 1.. Multiplicity loop
            if (LaneCounter(Index) < LaneCounter(n_lane)) then
               n_lane := Index;
            end if;
            -- n_lane � l'id del lane meno trafficato
         end loop;
         LaneCounter(n_lane) := LaneCounter(n_lane) + 1;
         Free := Free - 1;
         CarLane(my_properties.CarId) := n_lane;

         pragma Debug (CheckSurpass(Sector_Id, my_properties.CarId));

         Print("Auto "&my_properties.CarId'Img&" percorre Settore: "&Sector_Id'Img&", Tratto:"&n_lane'Img&".");
         if (IsBox = False) then
            requeue Corsie(Sector_Id)(n_lane).Demand with abort;
         else
            requeue Corsie(Sectors'Length + 1)(n_lane).Demand with abort;
         end if;
      end Enter;

      entry Release(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float) when Free < MaxCars is
         next_sect : Integer;
         a : Integer := 0;
         t : Integer := Sector_Id;
      begin
         if (ExitCount = 0) then
            Put_Line("Bug del software: invocata Relase senza aver invocato BookExit");
         end if;
         if (ExitArray(1).CarId = my_properties.CarId) then
            for Index in 2 .. ExitCount  loop
               ExitArray(Index -1) := ExitArray(Index);
            end loop;
            LaneCounter(CarLane(my_properties.CarId)) := LaneCounter(CarLane(my_properties.CarId)) -1;
            Free := Free + 1;
            ExitCount := ExitCount -1;
            if (ExitCount > 0) then
               if(ExitArray(1).Arrived = True) then
                  Changed := True;
               else
                  Changed := False;
               end if;
            end if;
            -- release terminata
         else
            for Index in 1 .. ExitCount loop
               if (ExitArray(Index).CarId = my_properties.CarId) then
                  ExitArray(Index).Arrived := True;
                  Print("DEBUG: Auto "&my_properties.CarId'Img&" ha superato dove non doveva..accodo in ExitLane");
                  Changed := False;
                  requeue ExitLane;
               end if;
            end loop;

         end if;
         ExitOrder(Sector_Id).Append(my_properties.CarId);
         -- calcolo il prossimo settore
         if (Sector_Id = Sectors'Length) then
            next_sect := 1;
         else
            next_sect := Sector_Id +1;
         end if;

         -- sosta ai box?
         if (PitStopRequest(my_properties.CarId) = true and then next_sect = PitStopSector) then
            sector := 0;
            PitStopRequest(my_properties.CarId) := False;
            requeue BoxSector.Enter;
         else
            sector := next_sect;
            requeue Sectors(next_sect).Enter;
         end if;
      end Release;

      entry ExitLane(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float) when Changed = True is
         next_sect : Integer;
      begin
         if (ExitArray(1).CarId = my_properties.CarId) then
            for Index in 2 .. ExitCount  loop
               ExitArray(Index -1) := ExitArray(Index);
            end loop;
            LaneCounter(CarLane(my_properties.CarId)) := LaneCounter(CarLane(my_properties.CarId)) -1;
            Free := Free + 1;
            ExitCount := ExitCount -1;
            ExitOrder(Sector_Id).Append(my_properties.CarId);
            if (ExitArray(1).Arrived = True) then
               Changed := True;
            else
               Changed := False;
            end if;
            -- calcolo il prossimo settore
            if (Sector_Id = Sectors'Length) then
               next_sect := 1;
            else
               next_sect := Sector_Id +1;
            end if;

            -- sosta ai box?
            if (PitStopRequest(my_properties.CarId) = true and then next_sect = PitStopSector) then
               sector := 0;
               requeue BoxSector.Enter;
            else
               sector := next_sect;
               requeue Sectors(next_sect).Enter;
            end if;
         else
            requeue ExitLane;
         end if;
      end ExitLane;

   end Sector;

   protected body Lane_T is
      entry Demand(my_properties : in CarProperties_T; sector : in out Integer; my_time : in out Time;  my_duration : out Duration; my_fuel: out Float; my_consumption : out Float; my_speed : in out Float)  when True is
         --start : Time;
         t : Duration;
      begin
         -- TEST: modifica codice per usare solo i tempi calcolati
         --start := Clock;
         -- END TEST
         CalculateDriveTime(my_properties , my_length ,my_level ,Weather,IsBox ,my_fuel, my_consumption,my_speed, t );
         --TEST
         my_time := my_time + t;--start + t;
         -- END TEST
         if (my_time > exit_time) then
            exit_time := my_time;
            exit_speed := my_speed;
         elsif (my_time < exit_time) then
            -- se l'auto � dietro, esce al tempo dell'auto che precede e al pi� alla sua stessa velocit�
            my_time := exit_time;
            if (my_speed > exit_speed) then
               my_speed := exit_speed;
            end if;
         end if;
         my_duration := t;--my_time - start;
      end Demand;
   end Lane_T;

   -- inserisce la macchina nel circuito, fornisce in ingresso le
   -- caratteristiche iniziali dell'auto. Quando questo metodo termina vuol
   -- dire che la macchina ha finito la gara (conclusa o per ritiro)
   procedure PutOnPitLane(my_CarProperties : CarProperties_T) is
      my_sect : Integer :=0;
      my_time : Time;
      my_fuel : Float;
      my_consumption : Float;
      my_duration : Duration;
      CarProperties : CarProperties_T := my_CarProperties;
      Speed : Float := 0.0;
      temp : Float;
      lap : Integer :=0;
      inter : Integer;
      old_lap: Integer :=0;
      old_sect: Integer := 0;
      old_time :Time;
      old_speed : Float := 0.0;
      use IntVector;
   begin
      Put_Line("DEBUG: chiamata putonpilane");
      Print("Car " & CarId_T'Image(CarProperties.CarId) & " registered");
      --Auto si mette nelle linee di partenza. Quando torner� da questa chiamata, la corsa per l'auto sar� iniziata
      PitLane(CarProperties.CarId).Wait;

      old_time := Clock;
      my_time := old_time;
      Put_Line("Auto "&CarProperties.CarId'Img&" partita");
      Simulator.Controller.GetRace.UpdateStatistics(CarProperties.CarId,CarProperties.Name, 0,1, (my_time - Time_Of(2009,9,21)), 0.0, 1);

      -- mi prenoto sull'ultimo settore
      Sectors(Sectors'Last).Enter(CarProperties, my_sect, my_time, my_duration, my_fuel, my_consumption, Speed);
      Sectors(Sectors'Last).BookExit(CarProperties, old_time);
      my_time := old_time;
      -- Partenza: ogni auto parte e notifica l0avvenuta partenza a quella in posizione successiva
      declare
         i: Integer := 1;
         success : Boolean := False;
      begin
         while ( i<= Integer(CarId_T'Last) and then success = False) loop
            PitLane(CarId_T(i)).Signal(True, success);
            Put_Line("Mando segnale all'auto "&i'Img&" con responso "&success'Img);
            i := i+1;
         end loop;
      end;
      Sectors(Sectors'Last).Release(CarProperties, my_sect, my_time, my_duration, my_fuel, my_consumption, Speed);

      Put_Line("DEBUG: Auto "&CarProperties.CarId'Img&" � partita e passata dal via.");
      --inizio la corsa iterando tra i vari Sector
      while (RetireRequest(CarProperties.CarId) = False) loop
         --calcolo il giro
         if (my_sect = 1) then
            lap := lap +1;
         end if;
         -- book
         if (my_sect /= 0) then
            Sectors(my_sect).BookExit(CarProperties, my_time);
         else
            BoxSector.BookExit(CarProperties, my_time);
         end if;

         -- aggiorno dati auto
         temp:= Float(CarProperties.CarFuel) + my_fuel;
         if (temp < Float(CarFuel_T'First)) then
            CarProperties.CarFuel := 0.0;
         elsif (temp > Float(CarFuel_T'Last)) then
            CarProperties.CarFuel := CarFuel_T'Last;
         else CarProperties.CarFuel := CarFuel_T(temp);
         end if;
         temp := Float(CarProperties.TiresConsumption) + my_consumption;
         if (temp < Float(TiresConsumption_T'First)) then
            CarProperties.TiresConsumption:=TiresConsumption_T'First ;
         elsif (temp > Float(TiresConsumption_T'Last)) then
            CarProperties.TiresConsumption := TiresConsumption_T'Last;
         else CarProperties.TiresConsumption := TiresConsumption_T(temp);
         end if;
         -- aggiorno l'auto remota
         Simulator.Controller.GetCar(CarProperties.CarId).UpdateProperties(CarId               => CarProperties.CarId,
                                                                           Tires                => CarProperties.Tires,
                                                                           TiresConsumption => CarProperties.TiresConsumption,
                                                                           CarFuel             => CarProperties.CarFuel,
                                                                           CarPerformance     => CarProperties.CarPerformance,
                                                                           Lap                  => lap,
                                                                           Sector                => my_sect,
                                                                           Speed                => Speed,
                                                                           SectorDuration      =>  my_time - old_time);-- my_duration);

         -- controllo se � un intermedio
         if (my_sect /= 0 and then Intermediate.Find_Index(Item  => my_sect, Index => Intermediate_Count_T'First) /=  No_Index ) then
            inter := Integer(Intermediate.Find_Index(Item  => my_sect, Index => Intermediate_Count_T'First));
         elsif ( my_sect = 0 and then Intermediate.Find_Index(Item  => PitStopSector, Index => Intermediate_Count_T'First) /=  No_Index) then
            inter := Integer(Intermediate.Find_Index(Item  => PitStopSector, Index => Intermediate_Count_T'First));
         else
            inter := 0;
         end if;

         old_lap := lap;
         old_sect := my_sect;
         old_time := my_time;
         old_speed := Speed;
         -- se sto percorrendo i box, notifico la cosa alla gara, che deve saperlo prima che l auto termini di percorrerli
         if (my_sect = 0) then
            Simulator.Controller.GetRace.UpdateCarStatus(CarProperties.CarId, old_lap, CarStatus_T(Box));
         end if;

         delay until my_time;

         --release
         if (my_sect /= 0) then
            Sectors(my_sect).Release(CarProperties, my_sect, my_time, my_duration, my_fuel, my_consumption, Speed);
         else
            BoxSector.Release(CarProperties, my_sect, my_time, my_duration, my_fuel, my_consumption, Speed);
         end if;

         -- TEST PER BUG
         if (old_sect = 0) then
            old_sect := PitStopSector;
            end if;
         --FINE TEST

         -- aggiorno le statistiche
         Simulator.Controller.GetRace.UpdateStatistics(CarProperties.CarId, CarProperties.Name,old_lap,old_sect, (old_time - Time_Of(2009,9,21)), old_speed, inter);
         --controllo se sono in grado di continuare la gara
         if (CarProperties.CarFuel <= 0.0 or CarProperties.TiresConsumption >= 1.0) then
            Print("LOG: Auto "&CarProperties.CarId'Img&" non puo' proseguire la gara.");
            Simulator.Controller.GetRace.Kill(CarProperties.CarId);
            RetireRequest(CarProperties.CarId) := True;
         end if;
      end loop;

      Put_Line("DEBUG: PutOnPitLane � ritornato dalla simulazione.. Auto "&CarProperties.CarId'Img&" ha terminato la corsa");
   end PutOnPitLane;


   function StartRace(n_cars : CarId_T; Randomized : Boolean) return Boolean is
   begin
      if (TrackStarted = False) then
         return False;
      end if;
      -- inizializzo PitStopRequest.
      for Index in 1 .. n_cars loop
         PitStopRequest(Index) := False;

      end loop;
      -- Modifico l'array in base al metodo di partenza deciso;
      if (Randomized = True) then
         Put_Line("Randomizzo la griglia di partenza");
         declare
            Temp : Semaphore_Ref;
            G: Generator;
            pos : CarId_T;
         begin
            Reset(G);
            for Index in 1 .. 48 loop
               for i in 1 .. CarId_T'Last loop
                  -- intero casuale equamente disribuito tra 1 e n_registered
                  pos := CarId_T(   1 + (Integer(Float(CarId_T'Last ) * Random(G)) mod Integer(CarId_T'Last))) ;
                  -- eseguo lo switch tra l elemento i-esimo e pos-esimo
                  Temp := PitLane(pos);
                  PitLane(pos) := PitLane(i);
                  PitLane(i) := Temp;
               end loop;
            end loop;
         end;
         Put_Line("Terminata randomizzazione griglia di partenza");
      end if;
      -- sveglio la prima auto in attesa su PitLane,
      declare
         i: Integer := 1;
         success : Boolean := False;
      begin
         while (i <= Integer(CarId_T'Last) and then success = False) loop
            PitLane(CarId_T(i)).Signal(True, success);
            Put_Line("Mando segnale all'auto "&i'Img&" con responso "&success'Img);
            i := i+1;
         end loop;
      end;
      Put_Line("Race started.");
      return True;
   end StartRace;

   -- Comunica di ritirare l'auto con id = CarId dal circuito
   procedure Kill(CarId: in CarId_T) is
   begin
      Put_Line("Killing car "&CarId'Img&" ...");
      RetireRequest(CarId) := True;
   end Kill;

   -- metodo di richiesta fermata ai box l'invocazione di questo metodo porta
   -- il circuito a far fare una sosta all'auto "CarId" appena possibile.
   procedure CallForPitStop(CarId: in CarId_T; CarFuel :CarFuel_T; Tires :Tires_T) is
   begin
      PitStopRequest(CarId) := True;
      PitStopData(CarId) := (CarFuel, Tires);
      Put_Line("Car "&CarId_T'Image(CarId)&" requires Pit Stop.");
   end CallForPitStop;

   procedure InitTrack is
   begin
      for i in CarId_T loop
         PitLane(i) := new Semaphore;
      end loop;
      -- Put_Line("griglia di partenza inizializzata");
   end InitTrack;

   function ReadTrackConf(confFile : String) return Boolean is
      Grammar : XML_Grammar;
      SReader  : Schema_Reader;
      SInput : File_Input;
      Input : File_Input;
      Reader : Schema.Dom_Readers.Tree_Reader;
      Doc : Document;
      List : Node_List;
      TrackList : Node_List;
      BoxList : Node_List;
      Settore : Node;
      A : Attr;
      Livello : Integer;
      Lunghezza : Natural;
      Multiplicity : Positive;
      Weather : String := "------";
      w : Weather_T;
      intermedi: Boolean := False;
   begin
      Set_Public_Id (SInput, "Schema-file");
      -- importo il file XMLSchema
      Open ("schema.xsd", SInput);
      Parse (SReader, SInput);
      Close (SInput);
      Grammar := Get_Created_Grammar (SReader);
      Global_Check (Grammar);

      Put_Line("DEBUG: Leggo configurazione da file xml");
      Set_Validating_Grammar (Reader, Grammar);


      Set_Public_Id (Input, "Track file");
      Open (confFile, Input);
      Set_Feature (Reader, Sax.Readers.Validation_Feature, false);
      Set_Feature (Reader, Sax.Readers.Namespace_Feature, false);
      Set_Feature (Reader, Sax.Readers.Schema_Validation_Feature, True);

      Parse (Reader, Input);
      Close (Input);
      Doc := Get_Tree (Reader);
      TrackList := Get_Elements_By_Tag_Name(Doc, "track");

      if (Length(TrackList) /= 1) then
         Put_Line("Errore nel file XML. Ogni file deve contenere esattamente un elemento track");
      end if;
      List := Get_Elements_By_Tag_Name(Doc, "sector");
      Put_Line("DEBUG TRACK:ci sono "&Length(List)'Img&" settori");
      Sectors := new Sector_Array_T(1 .. Length(List));
      Corsie:= new Corsie_Array_T (1 .. Length(List) + 1);
      ExitOrder := new ExitOrder_T(1 .. Length(List) +1);

      for Index in 1 .. Length (List) loop
         Settore := Item (List, Index - 1);
         A := Get_Named_Item (Attributes (Settore), "level");
         Livello := Integer'Value  (Node_Value(A)) ;
         A := Get_Named_Item (Attributes (Settore), "length");
         Lunghezza := Natural'Value (Node_Value(A));
         A := Get_Named_Item (Attributes (Settore), "multiplicity");
         Multiplicity := Positive'Value (Node_Value(A));
         -- Creo l'array Corsie_T relativo al settore
         Corsie(Index) := new Corsie_T( 1 .. Multiplicity);
         ExitOrder(Index) := new CarIdVector_T.Vector;
         --weather
         A := Get_Named_Item (Attributes (Settore), "weather");
         -- Se l'attributo weather non � specificato, il settore � asciutto
         if( A = null or else Node_Value(A) = "dry") then
            Put_Line("dry sector");
            w := Dry;
         else
            Put_Line("wet sector");
            w := Wet;
         end if;

         A := Get_Named_Item (Attributes (Settore), "intermediate");

         if( A /= null and then Boolean'Value (Node_Value(A)) = True) then
            Intermediate.Append(Index);
            Put_Line("intertempo");
         end if;

         --Creo il settore e i lane
         Sectors(Index) := new Sector(Index,Multiplicity,Lunghezza,Livello,False);
         for i in 1 .. Multiplicity loop
            Corsie(Index)(i) := new Lane_T(Lunghezza,Livello,w,False);
         end loop;
         Put_Line ("creato settore "& Index'Img);

         BoxList := Child_Nodes( Settore);
         if (Length(BoxList) /= 0) then
            Put_Line("Inizializzo box");
            PitStopSector := Index;

            A := Get_Named_Item (Attributes (Item(BoxList,1)), "length");
            Lunghezza := Natural'Value (Node_Value(A));
            BoxSector := new Sector(Index, MaxId, Lunghezza, Livello, True);
            -- Creo l'array Corsie_T relativo ai box ..  Moltiplicit� MaxId
            Corsie(Length(List) + 1) := new Corsie_T( 1 .. MaxId);
            ExitOrder(Length(List) + 1) := new CarIdVector_T.Vector;
            for i in 1 .. MaxId loop
               Corsie(Length( List ) + 1)(i) := new Lane_T(Lunghezza,Livello,w,True);
            end loop;
            Put_Line("Trovati box nel sttore "& PitStopSector'Img&" di lunghezza "&Lunghezza'Img);
         end if;
      end loop;
      -- Inizializzo PitLane
      --   for i in CarId_T loop
      -- PitLane(i) := new Semaphore;
      -- end loop;
      Free (List);
      Free (Reader);
      TrackStarted := True;
      return True;
   exception
      when Error : others =>
         Print_NoLock("Errore nel file di configurazione");
         Print_NoLock(Exception_Message(Error));
         return False;
   end ReadTrackConf;

end Simulator.Track;
