with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;

with Simulator.Controller;
with Simulator.Track;

with Simulator.Car.Gui; use Simulator.Car.Gui;

with Glib; use Glib;
with Gtk.Label; use Gtk.Label;
with Gtk.Button; use Gtk.Button;
with Gtk.Scale; use Gtk.Scale;

package body Simulator.Car is
   package DurationIO is new Ada.Text_IO.Fixed_Io(Duration);
   use Simulator;

   -- Oggetto che iidentifica l'auto. Non pu� essere locale alle funzioni.
   Own_Car : aliased Car_Type;

   --Task da eseguire solo quando Own_Car � inizializzato completamente
   task type Racer;
   type Racer_Access is access Racer;
   Racer_Instance : Racer_Access;

   task body Racer is
   begin
      Simulator.Track.PutOnPitLane(Own_Car.CarProperties);
      Print("Simulazione conclusa.");
   end Racer;

   function Init(Tires: Tires_T;
                 CarWeight: CarWeight_T;
                 CarFuel: CarFuel_T;
                 CarPerformance: CarPerformance_T;
                Name : String) return Boolean is
      use Simulator.Controller;
      Success : Boolean;
      Registered : Boolean := False;
      id : CarId_T;
      str : String := "------";
   begin
      --iscrivo l'auto alla gara e ottengo l'id dell'auto
      Print_NoLock("LOG: Registro l'auto alla corsa.");
      if ( Simulator.Controller.GetRace /= null ) then
         Simulator.Controller.GetRace.CarRegister(id, Success);
      else
         Print_NoLock("ERROR: Nessuna Gara disponibile." );
         return false;
      end if;

      if (Success = True) then
         --creo l'istanza di CarProperties_T
         Own_Car.CarProperties := (id,Name,Tires,0.0,CarFuel,CarPerformance);
	Registered := Simulator.Controller.RegisterCar (Own_Car'Access, id);
         -- aggiorno GUI con il nuovo id
         Set_Text(ValId, Own_Car.CarProperties.CarId'Img);
         Print_NoLock("LOG: Auto inizializzata." );
         Racer_Instance := new Racer;
         SetWinTitle("Car "&id'Img);
         return True;
      else
         return False;
      end if;
   end Init;

   --PROCEDURE DI SERVIZIO

   -- chiamata a procedura di circuito che avverte di fermarsi
   -- ai box appena possibile
   procedure CallForPitStop(CarFuel :CarFuel_T; Tires :Tires_T) is
   begin
      Simulator.Track.CallForPitStop(Own_Car.CarProperties.CarId,CarFuel, Tires);
   end CallForPitStop;

   -- chiamata a procedura di gara che permette di ritirare
   -- un' auto dalla corsa
   CurrentLap : Integer := 0;
   FuelLevel : Float :=0.0;
   TyresLevel : Float := 0.0;
   PitStopRequest : Boolean := False;
   PitStopInThisLap : Boolean := False;

   procedure Kill is
   begin
      Simulator.Controller.GetRace.Kill(Own_Car.CarProperties.CarId);
   end Kill;

   procedure UpdateProperties(This : access Car_Type;
                              CarId: CarId_T;
                              Tires: Tires_T;
                              TiresConsumption: TiresConsumption_T;
                              CarFuel: CarFuel_T;
                              CarPerformance: CarPerformance_T;
                              Lap: Integer;
                              Sector : Natural;
                              Speed : Float;
                              SectorDuration : Duration) is
      str : String := "aaaaaaaaaa";
      time : String := "bbbbbbbbb";
   begin
      This.CarProperties.CarId := CarId;
      This.CarProperties.Tires := Tires;
      This.CarProperties.TiresConsumption := TiresConsumption;
      This.CarProperties.CarFuel := CarFuel;
      This.CarProperties.CarPerformance := CarPerformance;

      if (Lap /= CurrentLap or else Lap =0) then
        if (Lap > 1 and then PitStopInThisLap = False) then
         SetUsage(FuelLevel - Float(Get_Value(ValFuel)),Float(Get_Value(ValTyresConsum)) - TyresLevel) ;
        end if;
            FuelLevel :=  Float(Own_Car.CarProperties.CarFuel);
         TyresLevel :=Float(Own_Car.CarProperties.TiresConsumption);
         CurrentLap := Lap;
         PitStopInThisLap := False;
      end if;


      Set_Value(ValFuel, GDouble(Own_Car.CarProperties.CarFuel));
      Set_Value(ValTyresConsum, GDouble(Own_Car.CarProperties.TiresConsumption));

      if ((Get_Value(ValFuel) <= Get_Value(ValFuelLimit) or else Get_Value(ValTyresConsum) >= Get_Value(ValTyresConsumLimit)) and then PitStopRequest = False) then
         Clicked(ButtBox);
         PitStopRequest := True;
        end if;


      Put(str, Float(Speed*3.6), Aft => 3, Exp => 0);
      DurationIO.Put(time, SectorDuration, Aft => 3);

      if (Sector = 0) then
         -- settore box
         Print("LOG: Giro: "& Lap'Img &" Tratto: Box, "& time &"s, "& str &"km/h.");
         PitStopRequest := False;
         PitStopInThisLap := True;
      else
          -- settore normale
         Print("LOG: Giro: "& Lap'Img &" Tratto: "& Sector'Img &", "& time &"s, "& str &"km/h.");
      end if;
      end UpdateProperties;


end Simulator.Car;
