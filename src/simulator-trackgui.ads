-- include GTK
with Gdk.Event; use Gdk.Event;
with Gtk.Widget, Gtk.Handlers; use Gtk.Widget, Gtk.Handlers;

with Gtk.Label; use Gtk.Label;
with Gtk.Button; use Gtk.Button;
with Gtk.File_Chooser; use Gtk.File_Chooser;
with Gtk.File_Chooser_Button; use Gtk.File_Chooser_Button;
with Gtk.Text_View; Use Gtk.Text_View;

package Simulator.TrackGui is
   -- elementi GUI usati da piu' procedure
   ButtonFile : Gtk_File_Chooser_Button;
   ButtonLoad : Gtk_Button;
   Console : Gtk_Text_View;

   procedure Init_Track_Gui;
   procedure Print(str : String);
   procedure Print_NoLock(str : String);
   procedure Load_Config (Widget : access Gtk_Widget_Record'Class);
end Simulator.TrackGui;
