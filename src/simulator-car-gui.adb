with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Simulator.Car; use Simulator.Car;
with Simulator.Gui; use Simulator.Gui;

-- include GTK
with Gdk.Threads;
with Gtk.Main;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;
with Gtk.Frame; use Gtk.Frame;
with Gtk.Separator; use Gtk.Separator;
with Gtk.Enums; use Gtk.Enums;
with Gtk.Alignment; use Gtk.Alignment;
with Gtk.Box; use Gtk.Box;
with Gtk.Table; use Gtk.Table;
with Gtk.Text_Buffer; Use Gtk.Text_Buffer;
with Gtk.Text_Iter; use Gtk.Text_Iter;
with Gtk.Text_Mark; use Gtk.Text_Mark;

package body Simulator.Car.Gui is

   procedure Init_Car_Gui is
      VBox1 : Gtk_Box;
      HBox : Gtk_Box;
      Frame : Gtk_Frame;
      Sep : Gtk_Separator;
      Table : Gtk_Table;
      TableBox : Gtk_Table;
      align : Gtk_Alignment;
      Label : Gtk_Label;
      LabelId : Gtk_Label;
      LabelName : Gtk_Label;
      LabelTyres : Gtk_Label;
      LabelPerformance : Gtk_Label;
      LabelFuel : Gtk_Label;
      LabelFuelLimit : Gtk_Label;
      LabelTyresConsum : Gtk_Label;
      LabelTyresConsumLimit : Gtk_Label;
      Scroller : Gtk_Scrolled_Window;
      b : Boolean := False;
   begin
      -- Inizializzo GTK e la finestra principale
      Gdk.Threads.G_Init;
      Gdk.Threads.Init;
      Gtk.Main.Init;
      Gtk.Window.Gtk_New (Window);
      -- Bordo della finestra e bottone centrale
      Gtk.Window.Set_Border_Width (Window, 5);
      Set_Size_Request (Window, 470, 500);

      Gtk_New(Table, 8, 2, False);
      Gtk_New(LabelId, "Id:");
      Gtk_New(ValId, "--");
      Gtk_New(LabelName, "Name:");
      Gtk_New(ValName);
      Set_Max_Length(ValName, 15);
      Set_Text(ValName, "pilot");
      Gtk_New(LabelTyres, "Tyres type:");
      Gtk_New_Text(ValTyres);
      Append_Text(ValTyres, "Slick");
      Append_Text(ValTyres, "Rain");
      Set_Active(ValTyres, 0);
      Gtk_New(LabelPerformance, "Performance:");
      Gtk_New_Hscale(ValPerformance, 1.0, 100.0, 1.0);
      Set_Size_Request(ValPerformance, 150);
      Gtk_New(LabelFuel, "Fuel:");
      Gtk_New_Hscale(ValFuel, 0.0, 150.0, 1.0);
      Set_Value(ValFuel, 50.0);
      Set_Size_Request(ValFuel, 150);
      Gtk_New(LabelFuelLimit, "Fuel Limit:");
      Gtk_New_Hscale(ValFuelLimit, 0.0, 150.0, 1.0);
      Set_Value(ValFuelLimit, 10.0);
      Set_Size_Request(ValFuel, 150);
      Gtk_New(LabelTyresConsum, "Tyres Consumption:");
      Gtk_New_Hscale(ValTyresConsum, 0.0, 1.0, 0.01);
      Set_Size_Request(ValTyresConsum, 150);
      Set_Sensitive(ValTyresConsum, False);
      Gtk_New(LabelTyresConsumLimit, "Tyres Limit:");
      Gtk_New_Hscale(ValTyresConsumLimit, 0.0, 1.0, 0.01);
      Set_Value(ValTyresConsumLimit, 0.9);
      Set_Size_Request(ValTyresConsumLimit, 150);

      Gtk_New_Text(ValTyresBox);
      Append_Text(ValTyresBox, "Slick");
      Append_Text(ValTyresBox, "Rain");
      Set_Active(ValTyresBox, 0);
      Gtk_New(ValFuelBox, 0.0, 150.0, 1.0);
      Set_Value(ValFuelBox, 30.0);
      Gtk_New (ButtBox, "Force PitStop");
      Set_Sensitive(ButtBox, False);
      Gtk_New(LabelUsage, "<b>Lap usage</b>:" & ASCII.LF &"      Fuel: --" & ASCII.LF & "      Tyres: --");
      Set_Use_Markup(LabelUsage, true);
      Gtk_New (ButtKill, "Retire Car");

      Gtk_New (Button, "Start Car");

      Gtk_New (Scroller);
      Set_Policy  (Scroller, Policy_Automatic, Policy_Always);
      Gtk_New (Console);
      Set_Editable (Console, False);
      Set_Cursor_Visible (Console, False);
      Set_Overwrite (Console, False);
      Set_Wrap_Mode (Console, Wrap_Word);

      -- impacchetto nel layout
      Gtk_New_Vbox (VBox1, False, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelId);
      Attach(Table,align, 0, 1, 0, 1, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValId);
      Attach(Table,align, 1, 2, 0, 1, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelName);
      Attach(Table,align, 0, 1, 1, 2, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValName);
      Attach(Table,align, 1, 2, 1, 2, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelTyres);
      Attach(Table, align, 0, 1, 2, 3, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValTyres);
      Attach(Table,align, 1, 2, 2, 3, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelPerformance);
      Attach(Table, align, 0, 1, 3, 4, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 1.0, 0.0);
      Add(align, ValPerformance);
      Attach(Table,align, 1, 2, 3, 4, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelFuel);
      Attach(Table, align, 0, 1, 4, 5, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 1.0, 0.0);
      Add(align, ValFuel);
      Attach(Table,align, 1, 2, 4, 5, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelFuelLimit);
      Attach(Table, align, 0, 1, 5, 6, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 1.0, 0.0);
      Add(align, ValFuelLimit);
      Attach(Table,align, 1, 2, 5, 6, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelTyresConsum);
      Attach(Table, align, 0, 1, 6, 7, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 1.0, 0.0);
      Add(align, ValTyresConsum);
      Attach(Table,align, 1, 2, 6, 7, Fill, Fill, 5, 5);
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, LabelTyresConsumLimit);
      Attach(Table, align, 0, 1, 7, 8, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 1.0, 0.0);
      Add(align, ValTyresConsumLimit);
      Attach(Table,align, 1, 2, 7, 8, Fill, Fill, 5, 5);

      Gtk_New(TableBox, 7, 2, False);
      Gtk_New(Label, "Tyres:");
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, Label);
      Attach(TableBox, align, 0, 1, 0, 1, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValTyresBox);
      Attach(TableBox, align, 1, 2, 0, 1, Fill, Fill, 5, 5);
      Gtk_New(Label, "Fuel:");
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, Label);
      Attach(TableBox, align, 0, 1, 1, 2, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValFuelBox);
      Attach(TableBox, align, 1, 2, 1, 2, Fill, Fill, 5, 5);
      Gtk_New(align, 0.5, 0.5, 0.0, 0.0);
      Add(align, ButtBox);
      Attach(TableBox, align, 0, 2, 2, 3, Fill, Fill, 5, 5);
      Gtk_New(align, 0.5, 0.5, 0.0, 0.0);
      Gtk_New_Hseparator (Sep);
      Add(align, Sep);
      Set_Size_Request(Sep, 50);
      Attach(TableBox, align, 0, 2, 3, 4, Fill, Fill, 5, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, LabelUsage);
      Attach(TableBox, align, 0, 2, 4, 5, Fill, Fill, 5, 5);
      Gtk_New(align, 0.5, 0.5, 0.0, 0.0);
      Gtk_New_Hseparator (Sep);
      Add(align, Sep);
      Set_Size_Request(Sep, 50);
      Attach(TableBox, align, 0, 2, 5, 6, Fill, Fill, 5, 5);
      Gtk_New(align, 0.5, 0.8, 0.0, 0.0);
      Add(align, ButtKill);
      Attach(TableBox, align, 0, 2, 6, 7, Fill, Expand, 5, 5);

      Gtk_New (Frame, "Console");
      Add (Frame, TableBox);
      Gtk_New_Hbox (HBox, False, 5);
      Pack_Start(HBox, Table, True, True, 5);
      Pack_Start(HBox, Frame, True, True, 5);

      Pack_Start(VBox1, HBox, False, True, 5);
      Pack_Start(VBox1, Button, False, True, 5);
      Add(Scroller, Console);
      Pack_Start(VBox1, Scroller, True, True, 5);

      Add(Window, VBox1);

      -- Connessione segnali
      -- collego il segnale di chiusura del Window Manager alla funzione Delete_Event
      Return_Handlers.Connect(Window, "delete_event", Return_Handlers.To_Marshaller (Delete_Event'Access));
      --  callback chiamato quando si invoca "destroy" su Window, o quando
      --  la funzione Delete_Event ritorna false
      Handlers.Connect(Window, "destroy", Handlers.To_Marshaller (Destroy'Access));
      -- callback del tasto start
      Handlers.Connect(Button, "clicked", Handlers.To_Marshaller (Start_Car'Access));
      -- callback del tasto PitStop
      Handlers.Connect(ButtBox, "clicked", Handlers.To_Marshaller (PitStop'Access));
      -- callback del tasto Retire_Car
      Handlers.Connect(ButtKill, "clicked", Handlers.To_Marshaller (Retire_Car'Access));
      -- Rendo tutto visibile
      Show_All (Window);

      -- Metto in attesa l'applicazione nel loop GTK
      Gdk.Threads.Enter;
      Gtk.Main.Main;
      Gdk.Threads.Leave;

      Put_Line ("GUI: terminato Gtk.Main.Main di auto. Termino l'applicazione.");
   end Init_Car_Gui;

   procedure Print(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Gdk.Threads.Enter;
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
      Gdk.Threads.Leave;
   end Print;

   procedure Print_NoLock(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
   end Print_NoLock;

   procedure SetWinTitle(str : String) is
   begin
      Set_Title(Window, str);
   end SetWinTitle;

   -- Callback per l'avvio dell'auto
   procedure Start_Car (Widget : access Gtk_Widget_Record'Class)
   is
      -- name deve essere lungo 15
      name :String :="               ";
      pragma Unreferenced (Widget);
      Success : Boolean;
      tyres : Tires_T;
   begin
      if (Get_Active_Text(ValTyres) = "Slick" ) then
         tyres := Slick;
      else
         tyres := Rain;
      end if;

      for i in 1 .. Get_Text(ValName)'Length loop
         name(i) := Get_Text(ValName)(i);
         end loop;
      Success := Simulator.Car.Init(tyres, 650,CarFuel_T(Get_Value(ValFuel)), CarPerformance_T(Get_Value(ValPerformance)),name);
      if( Success = True) then
         -- disabilito gli elementi grafici di inizializzazione
         Set_Sensitive(ValName, False);
         Set_Sensitive(ValTyres, False);
         Set_Sensitive(ValFuel, False);
         Set_Sensitive(ValPerformance, False);
         Set_Sensitive(Button, False);
         Set_Sensitive(ButtBox, True);
      else
         Print_NoLock("ERROR: impossibile iscriversi alla gara!");
      end if;
   end Start_Car;

   procedure PitStop (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
      tyres : Tires_T;
   begin
      Print_NoLock("GUI: Richiesto PitStop. Parametri: "&Get_Active_Text(ValTyresBox)&","&Integer(Get_Value(ValFuelBox))'Img&".");
      if (Get_Active_Text(ValTyresBox) = "Slick" ) then
         tyres := Slick;
      else
         tyres := Rain;
      end if;
      Simulator.Car.CallForPitStop(CarFuel_T(Get_Value(ValFuelBox)),tyres );
   end PitStop;

   procedure SetUsage(fuel : Float; tyres : Float)
   is
      strf : String := "-----";
      strt : String := "-----";
   begin
      Put(strf, fuel, Aft => 2, Exp => 0);
      Put(strt, tyres, Aft => 2, Exp => 0);
      Set_Label(LabelUsage, "<b>Lap usage</b>:" & ASCII.LF &"      Fuel: "& strf & ASCII.LF & "      Tyres: " & strt);
   end SetUsage;

   procedure Retire_Car (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin
      Print_NoLock ("GUI: Richiesto ritiro dell'auto.");
      Simulator.Car.Kill;
   end Retire_Car;
end Simulator.Car.Gui;
