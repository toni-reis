with Gtk; use Gtk;
with Gtk.Main; use Gtk.Main;
with Gdk.Threads;
with Simulator.Monitor.Gui; use Simulator.Monitor.Gui;

procedure Monitor is
   Monitor : Monitor_Access;
begin
   Gdk.Threads.G_Init;
   Gdk.Threads.Init;
   Gtk.Main.Init;
   Gtk_New (Monitor);
   Show_All (Monitor);

   Simulator.Monitor.Init;

   Gdk.Threads.Enter;
   Gtk.Main.Main;
   Gdk.Threads.Leave;


end Monitor;
