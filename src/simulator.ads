package Simulator is
   pragma Pure;

   -- Tipi per limitare i parametri della gara e delle auto
   MaxLevel : constant Positive := 5;
   MaxId : constant Positive := 30;
   type CarId_T is new Natural range 1 .. MaxId;

   type Tires_T is (Slick, Rain);
   type Weather_T is (Dry, Wet);
   type CarStatus_T is (Retired, Box, Running, Finished);

   type TiresConsumption_T is new Float range 0.0..1.00;

   MinWeight : constant Long_Integer := 400;
   MaxWeight : constant Long_Integer := 900;
   type CarWeight_T is new Long_Integer range MinWeight .. MaxWeight;

   MaxFuel : constant Float := 150.0; -- espresso in litri
   type CarFuel_T is new Float range 0.0 .. MaxFuel;

   type CarPerformance_T is new Natural range 1 .. 100;

   MaxMonitors : constant Natural := 10;

   -- Record che incapsula tutte le caratteristiche di un'auto
   type CarProperties_T is tagged
      record
         CarId: CarId_T;
         Name : String( 1 .. 15);
         Tires: Tires_T;
         TiresConsumption: TiresConsumption_T;
         CarFuel: CarFuel_T;
         CarPerformance: CarPerformance_T;
      end record;

   -- Tipi base per RATCW
   type Pool_Type is abstract tagged limited private;
   type Monitor_Pool_Type is abstract tagged limited private;

   -- Procedura remota esposta da Car
   procedure UpdateProperties
     (Pool    : access Pool_Type;
      CarId: CarId_T;
      Tires: Tires_T;
      TiresConsumption: TiresConsumption_T;
      CarFuel: CarFuel_T;
      CarPerformance: CarPerformance_T;
      Lap: Integer;
      Sector : Natural;
      Speed : Float;
      SectorDuration : Duration) is abstract;

   -- Procedura remota esposta da Monitor
   procedure UpdateMonitor
     (Pool : access Monitor_Pool_Type;
      Pos: Positive;
      CarId: CarId_T;
      Name : String;
      Lap: Natural;
      Intermediate: Natural;
      WakeupTime: Duration;
      Vel: Float;
      TimeElapsed : Duration) is abstract;
   procedure UpdateLapTime ( Pool : access Monitor_Pool_Type; Id: CarId_T;  LastLap : Duration; BestLap: Duration) is abstract;
   procedure UpdateCarStatus (Pool : access Monitor_Pool_Type; Id: CarId_T;  Status : CarStatus_T) is abstract;
   procedure Update_BestLap(Pool : access Monitor_Pool_Type; Id : CarId_T; Lap : Positive; Time : Duration) is abstract;
   procedure Update_MaxSpeed(Pool : access Monitor_Pool_Type; Id : CarId_T; Val : Float) is abstract;
   procedure EndRace(Pool : access Monitor_Pool_Type) is abstract;

   -- Interfaccia remota esposta da Gara
   type Remote_Race is task interface;
   procedure Kill(Remote: access Remote_Race; CarId: in CarId_T) is abstract;
   procedure SetStarted(Remote: access Remote_Race; start: Boolean) is abstract;
   procedure CarRegister(Remote: access Remote_Race;CarId: out CarId_T; Success : out Boolean) is abstract;
   procedure UpdateStatistics(Remote: access Remote_Race; Id : CarId_T; Name : String; Lap : Integer; Sector : Integer; WakeupTime : Duration; Speed : Float; Intermediate: Integer) is abstract;
   procedure UpdateCarStatus(Remote: access Remote_Race;Id : CarId_T; Lap: Integer; Status : CarStatus_T) is abstract;
private
   type Pool_Type is abstract tagged limited null record;
   type Monitor_Pool_Type is abstract tagged limited null record;
end Simulator;
