with Simulator; use Simulator;
with Simulator.Race; use Simulator.Race;

-- include GTK
with Gdk.Event; use Gdk.Event;
with Gtk.Widget, Gtk.Handlers; use Gtk.Widget, Gtk.Handlers;
with Gtk.Frame; use Gtk.Frame;
with Gtk.Label; use Gtk.Label;
with Gtk.Radio_Button; use Gtk.Radio_Button;
with Gtk.Spin_Button; use Gtk.Spin_Button;
with Gtk.Button; use Gtk.Button;
with Gtk.Text_View; Use Gtk.Text_View;

package Simulator.Race.Gui is
   -- elementi GUI usati da piu' procedure
   ValLaps : Gtk_Spin_Button;
   ValMaxCars : Gtk_Spin_Button;
   RandomCheck : Gtk_Radio_Button;
   IdCheck : Gtk_Radio_Button;
   ButtonInit : Gtk_Button;
   StatusFrame : Gtk_Frame;
   StatusLabel : Gtk_Label;
   ButtonStart : Gtk_Button;
   Console : Gtk_Text_View;

   procedure Init_Race_Gui(max : CarId_T);
   procedure Print(str : String);
   procedure Print_NoLock(str : String);
   procedure Init_Race (Widget : access Gtk_Widget_Record'Class);
   procedure UpdateRegistered(num: Integer);
   procedure Start_Race (Widget : access Gtk_Widget_Record'Class);
end Simulator.Race.Gui;
