with System;
with Simulator;
package Simulator.Race is
   pragma Remote_Types;
   MaxLaps : constant Positive := 30;
   type RetireRequest_T is array (CarId_T) of Boolean ;

   procedure Init(maxCars: CarId_T; laps: Integer);

   task type The_Race is new Simulator.Remote_Race with
      entry SetParameters(maxCars: CarId_T; laps : Integer);
      entry CarRegister  (id: out CarId_T; Success : out Boolean);
      -- permette all'auto che la invoca di ritirarsi dalla gara
      -- annuncia il ritiro al circuito, e notifica ai monitor l'evento
      entry Kill(CarId: in CarId_T);
      entry SetStarted(start: Boolean);

      -- procedura invocata da circuito ogni volta che un auto termina di
      -- percorrere un tratto. Aggiorna le statistiche che verranno
      -- mostrate nei monitor.
      entry UpdateStatistics(Id : CarId_T; Name : String; Lap : Integer; Sector : Integer; WakeupTime : Duration; Speed : Float; Intermediate: Integer);
   	entry UpdateCarStatus(Id : CarId_T; Lap: Integer; Status : CarStatus_T);
   end The_Race;

   function StartRace(Randomized : Boolean) return Boolean;

end Simulator.Race;
