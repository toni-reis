with Ada.Text_IO; use Ada.Text_IO;

with Simulator.Gui; use Simulator.Gui;

-- include GTK
with Glib; use Glib;
with Gdk.Threads;
with Gtk.Main;
with Gtk.Widget;
with Gtk.Window; use Gtk.Window;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;
with Gtk.Enums; use Gtk.Enums;
with Gtk.Alignment; use Gtk.Alignment;
with Gtk.Box; use Gtk.Box;
with Gtk.Table; use Gtk.Table;
with Gtk.Text_Buffer; Use Gtk.Text_Buffer;
with Gtk.Text_Iter; use Gtk.Text_Iter;
with Gtk.Text_Mark; use Gtk.Text_Mark;

package body Simulator.Race.Gui is
   procedure Init_Race_Gui(max : CarId_T) is
      Window : Gtk_Window;
      VBox : Gtk_Box;
      VBox2 : Gtk_Box;
      HBox : Gtk_Box;
      ParamsFrame : Gtk_Frame;
      StartFrame : Gtk_Frame;
      Table : Gtk_Table;
      align : Gtk_Alignment;
      Label : Gtk_Label;
      Scroller : Gtk_Scrolled_Window;
   begin
      -- Inizializzo GTK e la finestra principale
      Gdk.Threads.G_Init;
      Gdk.Threads.Init;
      Gtk.Main.Init;
      Gtk.Window.Gtk_New (Window);
      -- Bordo della finestra e bottone centrale
      Gtk.Window.Set_Border_Width (Window, 5);
      Set_Size_Request (Window, 400, 450);

      Gtk_New_Vbox (VBox, False, 5);
      Gtk_New_Vbox (VBox2, False, 5);
      Gtk_New_Hbox (HBox, True, 5);

      Gtk_New(Table, 2, 2, False);
      --- impostazione numero dei giri ---
      Gtk_New(Label, "Laps:");
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, Label);
      Attach(Table,align, 0, 1, 0, 1, Fill, Fill, 5, 5);
      Gtk_New(ValLaps, 1.0, 60.0, 1.0);
      Set_Value(ValLaps, 3.0);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValLaps);
      Attach(Table,align, 1, 2, 0, 1, Fill, Fill, 5, 5);
      --- impostazione numero partecipanti ----
      Gtk_New(Label, "Max Cars:");
      Gtk_New(align, 1.0, 0.5, 0.0, 0.0);
      Add(align, Label);
      Attach(Table,align, 0, 1, 1, 2, Fill, Fill, 5, 5);
      Gtk_New(ValMaxCars, 1.0, 30.0, 1.0);
      Set_Value(ValMaxCars, Gdouble(max));
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, ValMaxCars);
      Attach(Table,align, 1, 2, 1, 2, Fill, Fill, 5, 5);
      Gtk_New (RandomCheck, Group =>RandomCheck, Label => "Random");
      Gtk_New (IdCheck, Group =>RandomCheck, Label => "By ID");

      Gtk_New (ButtonInit, "Initialize");
      Gtk_New(StatusLabel, "Subscription are closed until Race is initialized.");
      Gtk_New (ButtonStart, "Start Race");
      Set_Sensitive(ButtonStart, False);

      Gtk_New (Scroller);
      Set_Policy  (Scroller, Policy_Automatic, Policy_Always);
      Gtk_New (Console);
      Set_Editable (Console, False);
      Set_Cursor_Visible (Console, False);
      Set_Overwrite (Console, False);
      Set_Wrap_Mode (Console, Wrap_Word);

      -- impacchetto nel layout
      Gtk_New (ParamsFrame, "Init Params");
      Gtk_New (StartFrame, "PitLane Order");
      Gtk_New (StatusFrame, "Subscription status");
      Set_Sensitive(StatusFrame, False);
      Pack_Start(VBox2, RandomCheck, False, True, 5);
      Pack_Start(VBox2, IdCheck, False, True, 5);
      Add (ParamsFrame, Table);
      Add (StartFrame, VBox2);
      Pack_Start(HBox, ParamsFrame, False, True, 5);
      Pack_Start(HBox, StartFrame, False, True, 5);
      Pack_Start(VBox, HBox, False, True, 5);
      Pack_Start(VBox, ButtonInit, False, True, 5);
      Gtk_New(align, 0.0, 0.5, 0.0, 0.0);
      Add(align, StatusLabel);
      Add (StatusFrame, align);
      Pack_Start(VBox, StatusFrame, False, True, 10);
      Pack_Start(VBox, ButtonStart, False, True, 5);
      Add(Scroller, Console);
      Pack_Start(VBox, Scroller, True, True, 5);

      Add(Window, VBox);

      -- Connessione segnali
      -- collego il segnale di chiusura del Window Manager alla funzione Delete_Event
      Return_Handlers.Connect(Window, "delete_event", Return_Handlers.To_Marshaller (Delete_Event'Access));
      --  callback chiamato quando si invoca "destroy" su Window, o quando
      --  la funzione Delete_Event ritorna false
      Handlers.Connect(Window, "destroy", Handlers.To_Marshaller (Destroy'Access));
      -- callback del tasto init
      Handlers.Connect(ButtonInit, "clicked", Handlers.To_Marshaller (Init_Race'Access));
      -- callback del tasto start
      Handlers.Connect(ButtonStart, "clicked", Handlers.To_Marshaller (Start_Race'Access));

      -- Rendo tutto visibile
      Show_All (Window);
      Print("GUI: Interfaccia utente gara inizializzata.");

      -- Metto in attesa l'applicazione nel loop GTK
      Gdk.Threads.Enter;
      Gtk.Main.Main;
      Gdk.Threads.Leave;
      Put_Line ("GUI: terminato Gtk.Main.Main di gara. Termino l'applicazione.");
   end Init_Race_Gui;

   procedure Print(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Gdk.Threads.Enter;
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
      Gdk.Threads.Leave;
   end Print;

   procedure Print_NoLock(str : String) is
      Iter : Gtk_Text_Iter;
      Mark : Gtk_Text_Mark;
   begin
      Get_End_Iter (Get_Buffer (Console), Iter);
      Insert (Get_Buffer (Console), Iter, str & ASCII.LF );
      Get_End_Iter (Get_Buffer (Console), Iter);
      Mark := Create_Mark(Buffer =>Get_Buffer (Console), Where =>Iter);
      Scroll_To_Mark(Console, Mark);
   end Print_NoLock;

   -- Callback per l'avvio dell'auto
   procedure Init_Race (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin
      Put_Line ("GUI: Callback Init_Race.");
      -- togliere dalla init il parametro MaxCars
      Simulator.Race.Init(CarId_T(Get_Value(ValMaxCars)),Integer(Get_Value(ValLaps)));
      Set_Text(StatusLabel, "Number of auto registered: 0");
      -- disabilito gli elementi grafici di inizializzazione
      Set_Sensitive(ValMaxCars, False);
      Set_Sensitive(ValLaps, False);
      Set_Sensitive(ButtonInit, False);
      Set_Sensitive(RandomCheck, False);
      Set_Sensitive(IdCheck, False);
      Set_Sensitive(StatusFrame, True);
      Set_Sensitive(ButtonStart, True);
   end Init_Race;

   procedure UpdateRegistered(num: Integer) is
   begin
      Set_Text(StatusLabel, "Number of auto registered: " & num'Img);
   end UpdateRegistered;

   procedure Start_Race (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
      i : Boolean;
   begin
      i := Simulator.Race.StartRace(Get_Active(RandomCheck));
      -- disabilito gli elementi grafici di inizializzazione
      if (i = True) then
         Set_Sensitive(StatusFrame, False);
         Set_Sensitive(ButtonStart, False);
      else
         Print_NoLock ("Impossibile iniziare la gara: track deve essere inizializzato e almeno un auto deve essere iscritta.");
         end if;
   end Start_Race;
end Simulator.Race.Gui;
