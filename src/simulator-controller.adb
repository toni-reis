package body Simulator.Controller is
   use Simulator;

   type Pool_Info is record
      Pool : Pool_Access;
      CarId  : CarId_T;
   end record;

   type Pool_Info_Table is array (1 .. Simulator.MaxId) of Pool_Info;
   type Monitor_Table is array (1 .. Simulator.MaxMonitors) of Monitor_Access;

   type Race_Info is record
      Race : Race_Access;
      RaceId  : RaceId_T;
   end record;

   type Race_Info_Table is array (1 .. 1) of Race_Info;

   protected Group is
      procedure Add  (Pool : in  Pool_Access; CarId : in CarId_T; success : out Boolean);
      procedure     GetAccess(Pool : out Pool_Access; CarId : CarId_T);
      procedure SetStop (i : Boolean);
   private
      Table      : Pool_Info_Table;
      Registered : Natural := 0;
      Stop       : Boolean := False;
   end Group;

   protected body Group is
      procedure Add  (Pool : in  Pool_Access; CarId : in CarId_T; success : out Boolean)  is
      begin
         if (Stop = False) then
         Registered := Registered + 1;
         Table (Registered) := (Pool, CarId);
         success := True;
      else
           success := False;
         end if;
      end Add;
      --+-----
      procedure GetAccess (Pool : out Pool_Access; CarId : CarId_T)  is
      begin
         for Index in 1 .. Registered loop
            if Table (Index).CarId = CarId then
               Pool := Table (Index).Pool;
               exit;
            end if;
         end loop;
      end GetAccess;
      procedure SetStop(i: Boolean) is
      begin
         Stop := i;
      end SetStop;


   end Group;

   --+--------------

   function RegisterCar
     (Pool : Pool_Access; CarId : CarId_T)
      return Boolean is
   success : Boolean;
   begin
      Group.Add (Pool, CarId,success);
      return success;
   end RegisterCar;

   function  GetCar
     (CarId : CarId_T)
      return Pool_Access is
      Pool : Pool_Access;
   begin
      Group.GetAccess (Pool, CarId);
      return Pool;
   end GetCar;

   -- RACE----------------------------------------------------------------------------------------
   protected RaceGroup is
      entry Add  (Race : in  Race_Access);
      entry GetAccess (Race : out Race_Access);
   private
      Table      : Race_Info_Table;
      Registered : Natural := 0;
      Full       : Boolean := False;
   end RaceGroup;

   protected body RaceGroup is
      entry Add  (Race : in  Race_Access) when true is
      begin
         Registered := Registered + 1;
         Table (Registered) := (Race, 1);
         Full := (Registered = 1);
      end Add;

      entry GetAccess (Race : out Race_Access) when true  is
      begin
         if (Registered > 0) then
            Race := Table (1).Race;
         else
            Race := null;
         end if;
      end GetAccess;
   end RaceGroup;

   function RegisterRace
     (Race : Race_Access)
      return Boolean is
   begin
      RaceGroup.Add (Race);
      return True;
   end RegisterRace;

   function  GetRace
     return Race_Access is
      Race : Race_Access;
   begin
      RaceGroup.GetAccess (Race);
      return Race;
   end GetRace;

   function SetStop (i : Boolean) return Boolean is
   begin
      Group.SetStop(i);
      return True;
   end SetStop;

   -- ----------------------------  MONITOR  ------------------------------
   protected Monitor_Group is
      procedure Add  (Pool : in  Monitor_Access);
      procedure Remove  (Pool : in  Monitor_Access; success : out Boolean);
      procedure GetAccess(Monitor : out Monitor_Access; Id : Positive);
      function GetRegistered return Natural;
   private
      Table      : Monitor_Table;
      Registered : Natural := 0;
   end Monitor_Group;

   protected body Monitor_Group is
      procedure Add  (Pool : in  Monitor_Access) is
      begin
         Registered := Registered + 1;
         Table (Registered) := (Pool);
      end Add;

      procedure Remove  (Pool : in  Monitor_Access; success : out Boolean) is
         pos : Positive;
         found : Boolean := false;
      begin
         for i in 1 .. Registered loop
            if (Table(i) = Pool) then
               pos := i;
               found := true;
            end if;
         end loop;

         if (found) then
            for i in pos .. Registered - 1 loop
               Table(i) := Table(i+1);
            end loop;
            Registered := Registered - 1;
         else
            -- not found
            null;
         end if;
         success := found;
      end Remove;

      procedure GetAccess(Monitor : out Monitor_Access; Id : Positive)  is
      begin
         for Index in 1 .. Registered loop
            if Index = Id then
               Monitor := Table(Index);
               exit;
            end if;
         end loop;
      end GetAccess;

      function GetRegistered return Natural is
      begin
         return Registered;
      end GetRegistered;

   end Monitor_Group;

   function RegisterMonitor
     (Monitor : Monitor_Access)
      return Boolean is
   begin
      Monitor_Group.Add (Monitor);
      return True;
   end RegisterMonitor;

   function UnregisterMonitor
     (Monitor : Monitor_Access)
      return Boolean is
      b : Boolean;
   begin
      Monitor_Group.Remove (Monitor, b);
      return b;
   end UnregisterMonitor;

   function GetMonitors
     return Natural is
   begin
      return Monitor_Group.GetRegistered;
   end GetMonitors;


   function   GetMonitor (MonitorId : Positive)
                          return Monitor_Access is
      Monitor : Monitor_Access;
   begin
      Monitor_Group.GetAccess (Monitor, MonitorId);
      return Monitor;
   end GetMonitor;

end Simulator.Controller;
