with Gdk.Event; use Gdk.Event;
with Gtk.Widget; use Gtk.Widget;

with Gtk.Window; use Gtk.Window;
with Gtk.Box; use Gtk.Box;
with Gtk.Frame; use Gtk.Frame;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;
with Gtk.Tree_Model; use Gtk.Tree_Model;
with Gtk.List_Store; use Gtk.List_Store;
with Gtk.Tree_Sortable; use Gtk.Tree_Sortable;
with Gtk.Tree_Model_Sort; use Gtk.Tree_Model_Sort;
with Gtk.Tree_View; use Gtk.Tree_View;
with Gtk.Tree_View_Column; use Gtk.Tree_View_Column;
with Gtk.Cell_Renderer; use Gtk.Cell_Renderer;
with Gtk.Cell_Renderer_Text; use Gtk.Cell_Renderer_Text;
with Gtk.Label; use Gtk.Label;
with Gtk.Alignment; use Gtk.Alignment;
with Gtk.Button; use Gtk.Button;

with Simulator.Gui; use Simulator.Gui;

package Simulator.Monitor.Gui is
   type ChangePosition_T is array (CarId_T) of Integer;
   ChangePosition : ChangePosition_T;
   type Monitor_Record is new Gtk_Window_Record with record
      -- varabili logica monitor
      LastLap : Natural := 0;
      LastInt : Natural := 0;
      LastTime : Duration := 0.0;
      Observing : Boolean := False;
      BestLap_Id : CarId_T := 1;
      BestLap_Lap : Natural := 0;
      BestLap : Duration := Duration'Last;
      MaxSpeed_Id : CarId_T := 1;
      MaxSpeed : Float := 0.0;

      -- variabili GUI
      Hbox1 : Gtk_Hbox;
      Hbox2 : Gtk_Hbox;
      Hbox3 : Gtk_Hbox;
      Vbox1 : Gtk_Vbox;
      Vbox2 : Gtk_Vbox;
      Frame1 : Gtk_Frame;
      Frame2 : Gtk_Frame;
      Frame3 : Gtk_Frame;
      Scrolledwindow1 : Gtk_Scrolled_Window;
      Scrolledwindow2 : Gtk_Scrolled_Window;
      Model1 : Gtk_List_Store;
      Model1_Sort : Gtk_Tree_Model_Sort;
      Model2 : Gtk_List_Store;
      Treeview1 : Gtk_Tree_View;
      Treeview2 : Gtk_Tree_View;
      Col : Gtk_Tree_View_Column;
      Text_R : Gtk_Cell_Renderer_Text;
      Label1 : Gtk_Label;
      Label2 : Gtk_Label;
      Label3 : Gtk_Label;
      Alignment1 : Gtk_Alignment;
      Alignment2 : Gtk_Alignment;
      Alignment3 : Gtk_Alignment;
      Int_Info : Gtk_Label;
      Lock : Gtk_Button;
      BestLap_Info : Gtk_Label;
      MaxSpeed_Info : Gtk_Label;

   end record;
   type Monitor_Access is access all Monitor_Record'Class;

   Ref : Monitor_Access;

   procedure Gtk_New (Monitor : out Monitor_Access);
   procedure Initialize (Monitor : access Monitor_Record'Class);
   procedure Update_Model (Pos: Positive;
                           CarId: CarId_T;
                           Name : String;
                           Lap: Natural;
                           Intermediate: Natural;
                           WakeupTime: Duration;
                           Vel: Float;
                           TimeElapsed: Duration);
   procedure UpdateLapTime ( Id: CarId_T;  LastLap : Duration; BestLap: Duration) ;
   procedure UpdateCarStatus ( Id: CarId_T;  Status : CarStatus_T);
   procedure Update_BestLap(Id : CarId_T; Lap : Positive; Time : Duration);
   procedure Update_MaxSpeed(Id : CarId_T; Val : Float);
   procedure ToggleLock (Widget : access Gtk_Widget_Record'Class);
   procedure EndRace;
end Simulator.Monitor.Gui;
