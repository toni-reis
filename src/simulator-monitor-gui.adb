with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Ada.Calendar.Formatting;

with Glib; use Glib;
with Gtk.Main; use Gtk.Main;
with Gtk; use Gtk;
with Gdk.Event; use Gdk.Event;
with Gdk.Types; use Gdk.Types;
with Gtk.Enums; use Gtk.Enums;
with Gdk.Threads; use Gdk.Threads;
with Gtkada.Dialogs; use Gtkada.Dialogs;

with Simulator.Statistics; use Simulator.Statistics;

package body Simulator.Monitor.Gui is
   package DurationIO is new Ada.Text_IO.Fixed_Io(Duration);

   function Delete_Event (Widget : access Gtk_Widget_Record'Class; Event  : Gdk_Event) return Boolean
   is
      pragma Unreferenced (Event);
      pragma Unreferenced (Widget);
   begin
      Simulator.Monitor.Quit;
      Gtk.Main.Main_Quit;
      Put_Line ("GUI: delete event occurred");
      return False;
   end Delete_Event;

   procedure Destroy (Widget : access Gtk_Widget_Record'Class) is
      pragma Unreferenced (Widget);
   begin
      Gtk.Main.Gtk_Exit(0);
   end Destroy;


   procedure Gtk_New (Monitor : out Monitor_Access) is
   begin
      Monitor := new Monitor_Record;
      Ref := Monitor;
      Simulator.Monitor.Gui.Initialize (Monitor);
   end Gtk_New;

   procedure Initialize (Monitor : access Monitor_Record'Class) is
      pragma Suppress (All_Checks);
      Pixmaps_Dir : constant String := "pixmaps/";
      Num : Gint;
   begin
      Gtk.Window.Initialize (Monitor, Window_Toplevel);
      Set_Border_Width (Monitor, 2);
      Set_Title (Monitor, "Monitor");
      Set_Position (Monitor, Win_Pos_None);
      Set_Modal (Monitor, False);
      Set_Resizable (Monitor, True);

      -- collego i segnali per la chiusura dell'applicazione
      Return_Handlers.Connect(Monitor, "delete_event", Return_Handlers.To_Marshaller (Delete_Event'Access));
      Handlers.Connect(Monitor, "destroy", Handlers.To_Marshaller (Destroy'Access));

      Gtk_New_Vbox (Monitor.Vbox2, False, 5);
      Gtk_New_Hbox (Monitor.Hbox1, False, 5);

      Gtk_New (Monitor.Frame1);
      Set_Label_Align (Monitor.Frame1, 0.0, 0.5);
      Set_Shadow_Type (Monitor.Frame1, Shadow_Etched_In);

      Gtk_New
        (Monitor.Alignment1, 0.5, 0.5, 1.0, 1.0);
      Set_Border_Width (Monitor.Alignment1, 5);

      Gtk_New (Monitor.Scrolledwindow1);
      Set_Policy (Monitor.Scrolledwindow1, Policy_Automatic, Policy_Automatic);
      Set_Shadow_Type (Monitor.Scrolledwindow1, Shadow_In);
      Set_Placement (Monitor.Scrolledwindow1, Corner_Top_Left);

      -- Creo il modello dati per la classifica
      Gtk_New (Monitor.Model1,
        (0 => GType_Int,     -- Pos
         1 => GType_Int,     -- Id
         2 => GType_String, -- Colore
         3 => GType_String, -- Name
         4 => GType_Int,     -- Lap
         5 => GType_Int,     -- Intermediate
         6 => GType_String,  -- Status
         7 => GType_Int,  -- # of PitStop
         8 => GType_String, -- Best Lap
         9 => GType_String -- Last Lap
        ));

      Gtk_New_With_Model (Monitor.Model1_Sort, Monitor.Model1);

      Gtk_New (Monitor.Treeview1, Monitor.Model1_Sort);
      Set_Sort_Column_Id (+(Monitor.Model1_Sort), 0 , Sort_Ascending);
      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Pos");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 0);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Id");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 1);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "background", 2);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Name");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 3);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Lap");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 4);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Int");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 5);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Status");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 6);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Stops");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 7);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Best Lap");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 8);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Last Lap");
      Num := Append_Column (Monitor.Treeview1, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 9);

      Set_Headers_Visible (Monitor.Treeview1, True);
      Set_Rules_Hint (Monitor.Treeview1, True);
      Set_Reorderable (Monitor.Treeview1, False);
      Set_Enable_Search (Monitor.Treeview1, False);

      Add (Monitor.Scrolledwindow1, Monitor.Treeview1);
      Add (Monitor.Alignment1, Monitor.Scrolledwindow1);
      Add (Monitor.Frame1, Monitor.Alignment1);
      Gtk_New (Monitor.Label1, "Ranking");
      Set_Alignment (Monitor.Label1, 0.5, 0.5);
      Set_Padding (Monitor.Label1, 0, 0);
      Set_Justify (Monitor.Label1, Justify_Left);
      Set_Line_Wrap (Monitor.Label1, False);
      Set_Selectable (Monitor.Label1, False);
      Set_Use_Markup (Monitor.Label1, True);
      Set_Use_Underline (Monitor.Label1, False);
      Set_Label_Widget (Monitor.Frame1,Monitor.Label1);

      Pack_Start
        (Monitor.Hbox1,
         Monitor.Frame1,
         Expand  => True,
         Fill    => True,
         Padding => 5);
      Gtk_New (Monitor.Frame2);
      Set_Label_Align (Monitor.Frame2, 0.0, 0.5);
      Set_Shadow_Type (Monitor.Frame2, Shadow_Etched_In);

      Gtk_New
        (Monitor.Alignment2, 0.5, 0.5, 1.0, 1.0);
      Set_Border_Width (Monitor.Alignment2, 5);

      Gtk_New (Monitor.Scrolledwindow2);
      Set_Policy (Monitor.Scrolledwindow2, Policy_Automatic, Policy_Automatic);
      Set_Shadow_Type (Monitor.Scrolledwindow2, Shadow_In);
      Set_Placement (Monitor.Scrolledwindow2, Corner_Top_Left);

      -- Creo il modello dati per gli intertempi
      Gtk_New (Monitor.Model2,
        (0 => GType_Int,     -- Pos
         1 => GType_Int,     -- Id
         2 => GType_String,    -- Distacco
         3 => GType_String     -- Velocita'
        ));

      Gtk_New (Monitor.Treeview2, Monitor.Model2);
      Set_Size_Request (Monitor.Treeview2, 250, 250);
      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Pos");
      Num := Append_Column (Monitor.Treeview2, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 0);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Id");
      Num := Append_Column (Monitor.Treeview2, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 1);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Time");
      Num := Append_Column (Monitor.Treeview2, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 2);

      Gtk_New(Monitor.Col);
      Gtk_New(Monitor.Text_R);
      Set_Title (Monitor.Col, "Vel");
      Num := Append_Column (Monitor.Treeview2, Monitor.Col);
      Pack_Start (Monitor.Col, Monitor.Text_R, True);
      Add_Attribute (Monitor.Col, Monitor.Text_R, "text", 3);

      Gtk_New_Vbox (Monitor.Vbox1, False, 0);
      Gtk_New_Hbox (Monitor.Hbox2, False, 0);

      Gtk_New (Monitor.Int_Info, "Lap: --     Intermediate: --");
      Set_Alignment (Monitor.Int_Info, 0.0, 0.5);
      Set_Padding (Monitor.Int_Info, 0, 0);
      Set_Justify (Monitor.Int_Info, Justify_Left);
      Set_Line_Wrap (Monitor.Int_Info, False);
      Set_Selectable (Monitor.Int_Info, False);
      Set_Use_Markup (Monitor.Int_Info, False);
      Set_Use_Underline (Monitor.Int_Info, False);

      Pack_Start
        (Monitor.Hbox2,
         Monitor.Int_Info,
         Expand  => True,
         Fill    => True,
         Padding => 0);

      -- Bottone Lock Statistics
      Gtk_New (Monitor.Lock, "Lock");
      -- callback del tasto Lock
      Handlers.Connect(Monitor.Lock, "clicked", Handlers.To_Marshaller (ToggleLock'Access));

      Pack_Start
        (Monitor.Hbox2,
         Monitor.Lock,
         Expand  => False,
         Fill    => False,
         Padding => 0);

      Pack_Start
        (Monitor.Vbox1,
         Monitor.Hbox2,
         Expand  => False,
         Fill    => False,
         Padding => 0);

      Set_Headers_Visible (Monitor.Treeview2, True);
      Set_Rules_Hint (Monitor.Treeview2, True);
      Set_Reorderable (Monitor.Treeview2, True);
      Set_Enable_Search (Monitor.Treeview2, False);

      Add (Monitor.Scrolledwindow2, Monitor.Treeview2);
      Add (Monitor.Alignment2, Monitor.VBox1);

      Pack_Start
        (Monitor.Vbox1,
         Monitor.Scrolledwindow2,
         Expand  => True,
         Fill    => True,
         Padding => 0);

      Add (Monitor.Frame2, Monitor.Alignment2);
      Gtk_New (Monitor.Label2, "Statistics");
      Set_Alignment (Monitor.Label2, 0.5, 0.5);
      Set_Padding (Monitor.Label2, 0, 0);
      Set_Justify (Monitor.Label2, Justify_Left);
      Set_Line_Wrap (Monitor.Label2, False);
      Set_Selectable (Monitor.Label2, False);
      Set_Use_Markup (Monitor.Label2, True);
      Set_Use_Underline (Monitor.Label2, False);
      Set_Label_Widget (Monitor.Frame2,Monitor.Label2);

      Pack_Start
        (Monitor.Hbox1,
         Monitor.Frame2,
         Expand  => False,
         Fill    => True,
         Padding => 5);

      Gtk_New (Monitor.Frame3);
      Set_Label_Align (Monitor.Frame3, 0.0, 0.5);
      Set_Shadow_Type (Monitor.Frame3, Shadow_Etched_In);

      Gtk_New
        (Monitor.Alignment3, 0.5, 0.5, 1.0, 1.0);
      Set_Border_Width (Monitor.Alignment3, 5);

      Add (Monitor.Frame3, Monitor.Alignment3);
      Gtk_New (Monitor.Label3, "Record");
      Set_Alignment (Monitor.Label3, 0.5, 0.5);
      Set_Padding (Monitor.Label3, 0, 0);
      Set_Justify (Monitor.Label3, Justify_Left);
      Set_Line_Wrap (Monitor.Label3, False);
      Set_Selectable (Monitor.Label3, False);
      Set_Use_Markup (Monitor.Label3, True);
      Set_Use_Underline (Monitor.Label3, False);
      Set_Label_Widget (Monitor.Frame3,Monitor.Label3);

      Gtk_New_Hbox (Monitor.Hbox3, False, 0);

      Gtk_New (Monitor.BestLap_Info, "Best Lap: -- : -- : ---   Car: -- on lap --");
      Set_Alignment (Monitor.BestLap_Info, 0.0, 0.5);

      Gtk_New (Monitor.MaxSpeed_Info, "Max Speed: ---,-- km/h   Car: --");
      Set_Alignment (Monitor.MaxSpeed_Info, 0.0, 0.5);

      Pack_Start
        (Monitor.Hbox3,
         Monitor.BestLap_Info,
         Expand  => True,
         Fill    => True,
         Padding => 5);

      Pack_Start
        (Monitor.Hbox3,
         Monitor.MaxSpeed_Info,
         Expand  => True,
         Fill    => True,
         Padding => 5);

      Add (Monitor.Alignment3, Monitor.HBox3);

      Pack_Start
        (Monitor.Vbox2,
         Monitor.Hbox1,
         Expand  => True,
         Fill    => True,
         Padding => 5);

      Pack_Start
        (Monitor.Vbox2,
         Monitor.Frame3,
         Expand  => False,
         Fill    => False,
         Padding => 5);

      Add (Monitor, Monitor.Vbox2);
      Set_Size_Request (Monitor, 800, 350);
   end Initialize;

   procedure   Update_Model (Pos: Positive;
                             CarId: CarId_T;
                             Name : String;
                             Lap: Natural;
                             Intermediate: Natural;
                             WakeupTime: Duration;
                             Vel: Float;
                             TimeElapsed: Duration) is
      Iter : Gtk_Tree_Iter;
      Iter1: Gtk_Tree_Iter;
      OldPos : Positive;
      StrTime : String := "tttttttttt";
      StrVel : String := "vvvvvvvv";
      m : Integer;
      s : Integer;
      c : Integer;
   begin
      Gdk.Threads.Enter;
      -- Put_Line("Update gui-monitor's data. Id: " & CarId'Img & ", Pos: " & Pos'Img & ".");
      -- cerco riga auto
      Iter := Get_Iter_First(Ref.Model1);
      while (Iter /= Null_Iter and then Get_Int(Ref.Model1, Iter, 1) /= GInt(CarId)) loop
         Next(Ref.Model1, Iter);
      end loop;

      -- aggiorno dati
      if (Iter /= Null_Iter) then
         -- aggiorno i valori
         OldPos := Positive(Get_Int(Ref.Model1, Iter, 0));
         Set (Ref.Model1, Iter, 0, GInt(Pos));
         Set (Ref.Model1, Iter, 1, GInt(CarId));
         -- alla partneza non segno zero ma un pi� chiaro 0 0
         if (Lap /= 0) then
            Set (Ref.Model1, Iter, 4, GInt(Lap));
            Set (Ref.Model1, Iter, 5, GInt(Intermediate));
         else
            Set (Ref.Model1, Iter, 4, 0);
            Set (Ref.Model1, Iter, 5, 0);
         end if;

            Set (Ref.Model1, Iter, 6, "running");

         if( Pos - OldPos = 0) then
            ChangePosition(CarId) := 0;
         else
            ChangePosition(CarId) :=ChangePosition(CarId)+ Pos - OldPos;
         end if;

         Put_Line("Posizione auto "&CarId'Img&" cambiata di: "&ChangePosition(CarId)'Img);
         -- coloro di verde la posizione
         if (ChangePosition(CarId) < 0) then
            Set (Ref.Model1, Iter, 2, "light green");
         else
            Set (Ref.Model1, Iter, 2, "white");
         end if;

         -- Aggiorno le posizioni delle macchine coinvolte nel sorpasso
         Iter := Get_Iter_First(Ref.Model1);

         while( Iter /= Null_Iter) loop
            if (Get_Int(Ref.Model1, Iter, 0) >= GInt(Pos) and then Get_Int(Ref.Model1, Iter, 0) < GInt(OldPos) and then Get_Int(Ref.Model1, Iter, 1) /= GInt(CarId)) then
               Set (Ref.Model1, Iter, 0, GInt(Get_Int(Ref.Model1, Iter, 0)+1));
               -- aggiorno cambio posizioni e coloro
               ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter, 1))) := ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter, 1))) +1;
               if ( ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter, 1)))< 0) then
                  -- coloro di verde la posizione
                  Set (Ref.Model1, Iter, 2, "light green");
               elsif ( ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter, 1)))= 0) then
                  Set (Ref.Model1, Iter, 2, "white");
               else
                  Set (Ref.Model1, Iter, 2, "red");
               end if;
            else
               null;
            end if;
            Next(Ref.Model1, Iter);
         end loop;
      else
         -- appendo in fondo alla lista
         Put_Line("Aggiungo a monitor Id: " & CarId'Img & ", Pos: " & Pos'Img & ".");
         Append(Ref.Model1, Iter);
         Set (Ref.Model1, Iter, 0, GInt(Pos));
         Set (Ref.Model1, Iter, 1, GInt(CarId));
         Set (Ref.Model1, Iter, 2, "white");
         Set (Ref.Model1, Iter, 3, Name);
           -- alla partneza non segno lap zero int 1 ma un pi� chiaro 0 0
         if (Lap /= 0) then
            Set (Ref.Model1, Iter, 4, GInt(Lap));
            Set (Ref.Model1, Iter, 5, GInt(Intermediate));
         else
            Set (Ref.Model1, Iter, 4, 0);
            Set (Ref.Model1, Iter, 5, 0);
         end if;
         Set (Ref.Model1, Iter, 6, "running");
         Set (Ref.Model1, Iter, 7, 0);
         Set (Ref.Model1, Iter, 8, "--");
         Set (Ref.Model1, Iter, 9, "--");
      end if;

      -- aggiorno i valori massimi
      if ((Lap > Ref.LastLap or ((Lap = Ref.LastLap) and (Intermediate > Ref.LastInt))) and Ref.Observing = False) then
         Ref.LastLap := Lap;
         Ref.LastInt := Intermediate;
         Ref.LastTime := WakeupTime;
         Set_Text(Ref.Int_Info, "Lap: "&Ref.LastLap'Img&"     Intermediate: "&Ref.LastInt'Img);
         -- e pulisco le statistiche
         Clear(Ref.Model2);
      end if;

      -- se giro e settore giusto aggiungo a statistics
      if ((Lap = Ref.LastLap) and (Intermediate = Ref.LastInt)) then
         --     Put_Line("Aggiungo a monitor di destra Id: " & CarId'Img & ", Pos: " & Pos'Img & ".");
         Insert(Ref.Model2, Iter, GInt(Pos)-1);
         Set (Ref.Model2, Iter, 0, GInt(Pos));
         Set (Ref.Model2, Iter, 1, GInt(CarId));
         if (Pos = 1) then
            -- Prima auto, scrivo il tempo di gara trascorso
            m := Integer(Float'Truncation(Float(TimeElapsed)/60.0));
            s := Integer(Float'Truncation(Float(TimeElapsed))) - m*60;
            c := Integer(Float'Truncation(Float(TimeElapsed * 1000))) - m*60000 -s *1000;
            Set (Ref.Model2, Iter, 2, m'Img & ":" & s'Img & ":" & c'Img);
         else
            DurationIO.Put(StrTime, (WakeupTime - Ref.LastTime), Aft => 3);
            Set (Ref.Model2, Iter, 2, "+ "&StrTime&" s");
         end if;
         Put(StrVel, Vel*3.6, Aft => 2, Exp => 0);
         Set (Ref.Model2, Iter, 3, StrVel&" km/h");
         Next(Ref.Model2, Iter);
         while (Iter /= Null_Iter) loop
            Set (Ref.Model2, Iter, 0, Get_Int(Ref.Model2, Iter, 0)+1);
            Put_Line("DEBUG: Calcolo le posizioni scorrette");
            Iter1 := Get_Iter_First(Ref.Model1);
            while (Iter1 /= Null_Iter) loop
               if ( Get_Int(Ref.Model1, Iter1, 1) = Get_Int(Ref.Model2, Iter, 1)) then
                  --Put_Line("DEBUG: Guadagno posizioni auto "&Get_Int(Ref.Model1, Iter1, 1)'Img&": "&ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter1, 1)))'Img);
                  --colora
                  if ( ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter1, 1)))< 0) then
                     -- coloro di verde la posizione
                     Set (Ref.Model1, Iter1, 2, "light green");
                  elsif ( ChangePosition(CarId_T(Get_Int(Ref.Model1, Iter1, 1)))= 0) then
                     Set (Ref.Model1, Iter1, 2, "white");
                  else
                     Set (Ref.Model1, Iter1, 2, "red");
                  end if;
               end if;
               Next(Ref.Model1, Iter1);
            end loop;
            Next(Ref.Model2, Iter);
         end loop;
      end if;
      Gdk.Threads.Leave;
   end Update_Model;

   procedure UpdateLapTime ( Id: CarId_T;  LastLap : Duration; BestLap: Duration) is
    Iter : Gtk_Tree_Iter;
     m : Integer;
      s : Integer;
      c : Integer;

   begin
      Gdk.Threads.Enter;
       Iter := Get_Iter_First(Ref.Model1);
      while (Iter /= Null_Iter and then Get_Int(Ref.Model1, Iter, 1) /= GInt(Id)) loop
         Next(Ref.Model1, Iter);
      end loop;

      -- aggiorno dati
      if (Iter /= Null_Iter) then
         -- aggiorno i valori
           if (BestLap /= Duration'Last) then
            m := Integer(Float'Truncation(Float(BestLap)/60.0));
            s := Integer(Float'Truncation(Float(BestLap))) - m*60;
            c := Integer(Float'Truncation(Float(BestLap * 1000))) - m*60000 -s *1000;
            Set (Ref.Model1, Iter, 8, m'Img & ":" & s'Img & ":" & c'Img);
         else
            Set (Ref.Model1, Iter, 8, "--");
         end if;
         if (LastLap /= Duration'Last) then
           m := Integer(Float'Truncation(Float(LastLap)/60.0));
            s := Integer(Float'Truncation(Float(LastLap))) - m*60;
            c := Integer(Float'Truncation(Float(LastLap * 1000))) - m*60000 -s *1000;
            Set (Ref.Model1, Iter, 9, m'Img & ":" & s'Img & ":" & c'Img);
         else
            Set (Ref.Model1, Iter, 9, "--");
         end if;
         end if;
      Gdk.Threads.Leave;
   end UpdateLapTime;

   procedure UpdateCarStatus ( Id: CarId_T;  Status : CarStatus_T) is
      Iter : Gtk_Tree_Iter;
   begin
      Gdk.Threads.Enter;
      -- search for the correct model row
       Iter := Get_Iter_First(Ref.Model1);
      while (Iter /= Null_Iter and then Get_Int(Ref.Model1, Iter, 1) /= GInt(Id)) loop
         Next(Ref.Model1, Iter);
      end loop;

      -- aggiorno dati
      if (Iter /= Null_Iter) then
         -- aggiorno lo stato
         case Status is
            when Retired =>
                 Set (Ref.Model1, Iter, 6, "retired");
            when Box =>
                 Set (Ref.Model1, Iter, 6, "BOX");
               -- aggiungo 1 al numero di soste
               Set (Ref.Model1, Iter, 7, Get_Int(Ref.Model1, Iter, 7)+1);
            when Finished =>
               Set (Ref.Model1, Iter, 6, "finished");
            when Running =>
                 Set (Ref.Model1, Iter, 6, "running");
            when others =>
               Set (Ref.Model1, Iter, 6, "uknown");
         end case;
      end if;
      Gdk.Threads.Leave;
      end UpdateCarStatus;

   procedure Update_BestLap(Id : CarId_T; Lap : Positive; Time : Duration) is
      m : Integer;
      s : Integer;
      c : Integer;
   begin
      -- Put_Line("Update monitor's best lap.");
      Ref.BestLap := Time;
      Ref.BestLap_Id := Id;
      Ref.BestLap_Lap := Lap;
      m := Integer(Float'Truncation(Float(Time)/60.0));
      s := Integer(Float'Truncation(Float(Time))) - m*60;
      c := Integer(Float'Truncation(Float(Time * 1000))) - m*60000 -s *1000;
      Set_Text(Ref.BestLap_Info, "Best Lap: "&m'Img&":"&s'Img&"."&c'Img &"   Car: "&Id'Img&" on lap "&Lap'Img);
   end Update_BestLap;

   procedure Update_MaxSpeed(Id : CarId_T; Val : Float) is
      StrSpeed : String := "vvvvvv";
   begin
      -- Put_Line("Update monitor's max speed.");
      Ref.MaxSpeed := Val;
      Ref.MaxSpeed_Id := Id;
      Put(StrSpeed, Val*3.6, Aft => 2, Exp => 0);
      Set_Text(Ref.MaxSpeed_Info, "Max Speed: "&StrSpeed&" km/h   Car: "&Id'Img);
   end Update_MaxSpeed;

   procedure ToggleLock (Widget : access Gtk_Widget_Record'Class) is
   begin
      Ref.Observing := not Ref.Observing;
      if (Ref.Observing) then
         Set_Label(Ref.Lock, "Unlock");
      else
         Set_Label(Ref.Lock, "Lock");
      end if;
   end ToggleLock;

   procedure EndRace is
      ans : Message_Dialog_Buttons;
   begin
      Gdk.Threads.Enter;
      ans := Message_Dialog("Race Finished !", Buttons => Button_OK);
      Gdk.Threads.Leave;
   end EndRace;

end Simulator.Monitor.Gui;
